package tests;

import com.facility.system.dal.*;
import com.facility.system.dal.hibernate.HibernateFacilityDAO;
import com.facility.system.model.facmaint.*;
import com.facility.system.model.facoperation.*;
import com.facility.system.model.facoverview.CFacilityOverseer;
import com.facility.system.model.facoverview.CGeneralFacility;
import com.facility.system.model.facoverview.COwnedFacility;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.user.COwner;
import com.facility.system.model.user.CRenter;
import com.facility.system.model.userservices.COwnerService;
import com.facility.system.model.userservices.CRenterService;
import com.facility.system.model.utility.Time;
import com.facility.system.model.utility.TimeStamp;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

/**
 * Created by zherr on 2/19/14.
 */
public class SpringTests {

    ApplicationContext appContext;
    // stub time stamps
    TimeStamp t1;
    TimeStamp t2;
    TimeStamp t3;
    TimeStamp t4;

    @Before
    public void setUp() throws Exception {
         appContext = new ClassPathXmlApplicationContext("META-INF/app-context.xml");
        t1 = new TimeStamp(2, 1, 2014, new Time(15, 30, 0, 0));
        t2 = new TimeStamp(2, 2, 2014, new Time(15, 30, 0, 0));
        t3 = new TimeStamp(3, 6, 2014, new Time(15, 30, 0, 0));
        t4 = new TimeStamp(3, 7, 2014, new Time(15, 30, 0, 0));
    }

    @Test
    public void testSpringTimeStamp() {
        Time springTime = (Time) appContext.getBean("time");
        springTime.setHours(5);
        springTime.setMinutes(30);
        springTime.setSeconds(0);
        System.out.println("Spring time = " + springTime);
        TimeStamp testTimeStamp = (TimeStamp) appContext.getBean("timeStamp");
        testTimeStamp.setTime(springTime);
        testTimeStamp.setDay(15);
        testTimeStamp.setMonth(9);
        testTimeStamp.setYear(1991);
        System.out.println("Spring TimeStamp = " + testTimeStamp);
    }

    @Test
    public void testSpringFacility() {
        CGeneralFacility springFac = (CGeneralFacility) appContext.getBean("facility");
        springFac.setAvailableCapacity(50);
        springFac.setFacilityDescription("A Spring facility");
        springFac.setFacilityId(999);
        springFac.setFacilityLocation("Chicago");
        springFac.setFacilityName("Spring Facility");
        springFac.setTotalCapacity(50);
        System.out.println("Spring Facility: \n" + springFac.getAvailableCapacity() + "\n" + springFac.getFacilityDescription() + "\n" + springFac.getFacilityLocation());
    }

    @Test
    public void testSpringFacilityOverseer() {
        CFacilityOverseer springFacOverseer = (CFacilityOverseer) appContext.getBean("facilityOverseer");
        springFacOverseer.setFacilityDAO((HibernateFacilityDAO)appContext.getBean("ownFacDAO"));
        System.out.println("...:::::Testing Facility Overseer:::::...");
        System.out.println("Listing facilities: ");
        List<IFacility> facs = new ArrayList<IFacility>();
        facs = springFacOverseer.listFacilities();
        for(IFacility fac : facs){
            System.out.println("Fac Name: " + fac.getFacilityName() + "\nFac Description: " + fac.getFacilityDescription() + "\nFac Location: " + fac.getFacilityLocation() + "Fac Capacity: " + fac.getTotalCapacity());
        }
        COwnedFacility fac = new COwnedFacility(2, "test facility", "a test fac", "Chicago", 500, 500, new COwner(987654321, "testOwner"));
        System.out.println("Adding new facility: ");
        springFacOverseer.addNewFacility(fac);
        System.out.println("Adding detail: ");
        fac.setFacilityLocation("Atlanta");
        springFacOverseer.addFacilityDetail(2, "test facility", "", "Atlanta", 500, 500, new COwner(123456789, "testOwner2"));
        System.out.println("Getting facility info: ");
        System.out.println(springFacOverseer.getFacilityInformation(fac));
        System.out.println("Requesting capacity: ");
        System.out.println("Capactiy: " + springFacOverseer.requestAvailableCapacity(fac));
        System.out.println("Removing facility: ");
        springFacOverseer.removeFacility(fac);
        System.out.println("Facility removed. \n");
    }

    @Test
    public void testSpringReservation() {
        Time time = new Time();
        time.setHours(5);
        time.setMinutes(30);
        time.setSeconds(0);
        TimeStamp testTimeStamp = new TimeStamp();
        testTimeStamp.setTime(time);
        testTimeStamp.setDay(15);
        testTimeStamp.setMonth(9);
        testTimeStamp.setYear(1991);
        CGeneralFacility fac = new CGeneralFacility();
        fac.setAvailableCapacity(50);
        fac.setFacilityDescription("A Spring facility");
        fac.setFacilityId(999);
        fac.setFacilityLocation("Chicago");
        fac.setFacilityName("Spring Facility");
        fac.setTotalCapacity(50);
        CReservation springReserve = (CReservation) appContext.getBean("reservation");
        springReserve.setEnd(testTimeStamp.toString());
        springReserve.setStart(testTimeStamp.toString());
        springReserve.setFacToReserve(fac);
        springReserve.setReserveId(999);
        springReserve.setTitle("Spring Reservation");
        springReserve.setNumOfParticipants(50);
        springReserve.setRenter(new CRenter(98989898, "testRenter"));
        assertNotNull(springReserve.getRenter());
        System.out.println(springReserve.getTitle() + ": \n" + springReserve.getReserveId() + "\n" + springReserve.getFacToReserve().getFacilityName() + "\n" +
                springReserve.getStart() + " - " + springReserve.getEnd() + "\n" + springReserve.getNumOfParticipants());
    }

    @Test
    public void testSpringInspection() {
        CGeneralFacility fac = new CGeneralFacility();
        fac.setAvailableCapacity(50);
        fac.setFacilityDescription("A Spring facility");
        fac.setFacilityId(999);
        fac.setFacilityLocation("Chicago");
        fac.setFacilityName("Spring Facility");
        fac.setTotalCapacity(50);
        CInspection springInspection = (CInspection) appContext.getBean("inspection");
    }

    @Test
    public void testSpringFacilityReserver() {
        CReservationDAO reservationDAO = new CReservationDAO();
        CFacilityReserver springFacReserver = (CFacilityReserver) appContext.getBean("facilityReserver");
        springFacReserver.setReservations(new ArrayList<IReservation>());
        springFacReserver.setReservationDAO(reservationDAO);
        assertNotNull(springFacReserver.getReservationDAO().getAllReservations(new CGeneralFacility(0, "", "", "", 0, 0)));
        assertTrue(springFacReserver.usageRate(new CGeneralFacility(0, "", "", "", 0, 0)) >= 0);
        Assert.assertNotNull(springFacReserver.getReservations());
    }

    @Test
    public void testSpringFacilityOperator() {
        CFacilityDAO facDAO = new CFacilityDAO();
        CReservationDAO resDAO = new CReservationDAO();
        CInspectionDAO inspectionDAO = new CInspectionDAO();
        CFacilityReserver reserver = new CFacilityReserver(new CReservationDAO(), new ArrayList<IReservation>());
        CFacilityOperator springOperator = (CFacilityOperator) appContext.getBean("facilityOperator");
        springOperator.setFacilityReserver(reserver);
        springOperator.setInspectionDAO(inspectionDAO);

        System.out.println("...:::::Testing Facility Operator:::::...");
        System.out.println("Assigning facility to use (adding reservation): ");
        IFacility fac2 = facDAO.getFacility(0);
        IReservation res = new CReservation(55, fac2, t1.toString(), t2.toString(), "Training", 50, new CRenter(1234567890, "renterTest"));
        springOperator.assignFacilityToUse(res);
        System.out.println("Listing reservations for facility that just had reservation assigned: \n");
        List<IReservation> reservations = new ArrayList<IReservation>();
        reservations = springOperator.listReservations(fac2);
        for(IReservation resv : reservations){
            System.out.println("Resv title: " + resv.getTitle());
        }
        System.out.println("Calculating usage rate of facility: " + springOperator.calcUsageRate(fac2));
        List<IInspection> inspections = new ArrayList<IInspection>();
        inspections = springOperator.listInspections(fac2);
        System.out.println("Listing inspections for facility: \n");
        for(IInspection inspect : inspections) {
            System.out.println("Inspection name: " + inspect.getInspectionName());
        }
        System.out.println("Testing if facility is in use between " + t1 + " and " + t2 + "\n");
        if(springOperator.IsInUseDuringInterval(fac2, t1, t2)) {
            System.out.println("Yes!");
        } else {
            System.out.println("No!");
        }
        System.out.println("Testing if facility is in use between " + t3 + " and " + t4 + "\n");
        if(springOperator.IsInUseDuringInterval(fac2, t3, t4)) {
            System.out.println("Yes!");
        } else {
            System.out.println("No!");
        }
        System.out.println("Vacating facility (then re-adding)");
        springOperator.vacateFacility(fac2);
        // Re-add all but last reservation
        for(int i = 0; i < reservations.size() - 1; ++i){
            resDAO.addReservation(reservations.get(i));
        }
    }

    @Test
    public void testSpringMaintenance() {
        CGeneralMaintenance springMaint = (CGeneralMaintenance) appContext.getBean("maintenance");
        springMaint.setCostOfMaintenance(500);
        springMaint.setMaintenanceDescription("spring maintenance");
        springMaint.setMaintenanceStart(t1.toString());
        springMaint.setMaintenanceEnd(t2.toString());
        springMaint.setMaintId(9999);
        assertTrue(springMaint.getMaintenanceDescription().equals("spring maintenance"));
        assertTrue(springMaint.getMaintId() == 9999);
    }

    @Test
    public void testSpringMaintRequest() {
        CMaintenanceRequest springMaintReq = (CMaintenanceRequest) appContext.getBean("maintRequest");
        springMaintReq.setFacilityRequested(new CGeneralFacility(999, "", "", "", 0, 0));
        springMaintReq.setMaintenance(new CGeneralMaintenance(99, 0, "", t1.toString(), t2.toString()));
        springMaintReq.setTimeRequested(t1.toString());
        assertNotNull(springMaintReq.getTimeRequested());
        assertNotNull(springMaintReq.getFacilityRequested());
        assertNotNull(springMaintReq.getMaintenance());
    }

    @Test
    public void testSpringMaintReqFactory() {
        CMaintRequestFactory springMaintReqFact = (CMaintRequestFactory) appContext.getBean("maintRequestFactory");
        assertNotNull(springMaintReqFact.createMaintenanceRequest(999, new CGeneralFacility(0, "", "", "", 0, 0),
                new CGeneralMaintenance(0, 0, "", t1.toString(), t2.toString()), t1, true));
    }

    @Test
    public void testSpringMainScheduler() {
        CGeneralMaintScheduler springMaintSched = (CGeneralMaintScheduler) appContext.getBean("maintScheduler");
        springMaintSched.setMaintenanceRequestDAO((CMaintenanceRequestDAO) appContext.getBean("maintReqDAO"));
        assertNotNull(springMaintSched.getMaintenanceRequestDAO());
        assertNotNull(springMaintSched.getMaintenanceRequestHistory());
        assertNotNull(springMaintSched.getMaintenanceRequestQueue());
    }

    @Test
    public void testSpringFacilityMaintainer() {
        IFacility fac2 = new CGeneralFacility(0, "", "", "", 0, 0);
        CFacilityMaintainer springFacilityMaintainer = (CFacilityMaintainer) appContext.getBean("facilityMaintainer");
        springFacilityMaintainer.setMaintenanceDAO((CMaintenanceDAO) appContext.getBean("maintDAO"));
        springFacilityMaintainer.setMaintenanceRequestDAO((CMaintenanceRequestDAO) appContext.getBean("maintReqDAO"));
        CGeneralMaintScheduler springMaintSched = (CGeneralMaintScheduler) appContext.getBean("maintScheduler");
        springMaintSched.setMaintenanceRequestDAO((CMaintenanceRequestDAO) appContext.getBean("maintReqDAO"));
        springFacilityMaintainer.setMaintenanceScheduler(springMaintSched);
        System.out.println("Calculating down time for facility: " + springFacilityMaintainer.calcDownTimeForFacility(fac2));
        System.out.println("Calculating maintenance cost for facility: " + springFacilityMaintainer.calcMaintenanceCostForFacility(fac2));
        List<IMaintenance> maintenances = new ArrayList<IMaintenance>();
        maintenances = springFacilityMaintainer.listFacilityProblems(fac2);
        System.out.println("Listing CURRENT facility maintenance problems: \n");
        for(IMaintenance maint : maintenances) {
            System.out.println("Maint name: " + maint.getMaintenanceDescription() + "\nCost of maint: " + maint.getCostOfMaintenance());
        }
        maintenances = springFacilityMaintainer.listMaintenance(fac2);
        System.out.println("Listing PAST facility maintenance: \n");
        for(IMaintenance maint : maintenances) {
            System.out.println("Maint name: " + maint.getMaintenanceDescription() + "\nCost of maint: " + maint.getCostOfMaintenance());
        }
        List<IMaintenanceRequest> maintReqs = new ArrayList<IMaintenanceRequest>();
        maintReqs = springFacilityMaintainer.listMaintRequest(fac2);
        System.out.println("Listing all maintenance REQUESTS");
        for(IMaintenanceRequest mq : maintReqs) {
            System.out.println("Maint request for facility: " + mq.getFacilityRequested().getFacilityName() + "\n Is Maintenance Request done?: " + mq.getIsDone() +
                    "\n Date requested: " + mq.getTimeRequested());
        }
        System.out.println("Making a maintenance request: ");
        IMaintenanceRequest maintReq = new CMaintenanceRequest(55, fac2, maintenances.get(0), t1.toString(), false);
        springFacilityMaintainer.makeFacilityMaintRequest(maintReq);
        System.out.println("Created.");
        System.out.println("Scheduling that maintenance request: ");
        springFacilityMaintainer.scheduleMaintenance(maintReq);
        maintReq = springFacilityMaintainer.getMaintenanceRequestDAO().getMaintenanceRequest(55);
        System.out.println("Scheduled?: " + maintReq.getIsDone());
        // remove
        springFacilityMaintainer.getMaintenanceRequestDAO().deleteMaintenanceRequest(maintReq);
    }

    @Test
    public void testSpringOwnedFacility() {
        COwnedFacility cOwnedFacility = (COwnedFacility) appContext.getBean("ownedFacility");
        assertNotNull(cOwnedFacility);
        cOwnedFacility.setOwner(new COwner(989898, "testOwner"));
        assertNotNull(cOwnedFacility.getOwner());
    }

    @Test
    public void testSpringOwnerService() {
        COwnerService sOwnerService = (COwnerService) appContext.getBean("ownerService");
        // TODO if necessary
    }

    @Test
    public void testSpringRenterService() {
        CRenterService sRenterService = (CRenterService) appContext.getBean("renterService");
        // TODO if necessary
    }
}
