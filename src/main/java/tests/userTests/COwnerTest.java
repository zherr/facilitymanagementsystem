package tests.userTests;

import com.facility.system.model.user.COwner;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Zach on 2/22/14.
 */
public class COwnerTest {

    COwner owner;

    @Before
    public void setUp() throws Exception {
        owner = new COwner(0, "Owner");
    }

    @Test
    public void testGetSsn() throws Exception {
        assertEquals(0, owner.getSsn());
    }

    @Test
    public void testSetSsn() throws Exception {
        owner.setSsn(213112312);
        assertEquals(213112312, owner.getSsn());
    }

    @Test
    public void testGetName() throws Exception {
        assertTrue(owner.getName().equals("Owner"));
    }

    @Test
    public void testSetName() throws Exception {
        owner.setName("test");
        assertTrue(owner.getName().equals("test"));
    }
}
