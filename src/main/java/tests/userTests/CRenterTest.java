package tests.userTests;

import com.facility.system.model.user.CRenter;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Zach on 2/22/14.
 */
public class CRenterTest {

    CRenter renter;

    @Before
    public void setUp() throws Exception {
        renter = new CRenter(0, "Renter");
    }

    @Test
    public void testGetSsn() throws Exception {
        assertTrue(renter.getSsn() == 0);
    }

    @Test
    public void testSetSsn() throws Exception {
        renter.setSsn(123123);
        assertTrue(renter.getSsn() == 123123);
    }

    @Test
    public void testGetName() throws Exception {
        assertTrue(renter.getName().equals("Renter"));
    }

    @Test
    public void testSetName() throws Exception {
        renter.setName("Renter2");
        assertTrue(renter.getName().equals("Renter2"));
    }
}
