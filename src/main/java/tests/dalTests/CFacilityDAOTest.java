package tests.dalTests;

import com.facility.system.dal.CFacilityDAO;
import com.facility.system.dal.IFacilityDAO;
import com.facility.system.model.facoverview.CGeneralFacility;
import com.facility.system.model.facoverview.IFacility;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 1/30/14.
 */
public class CFacilityDAOTest {

    IFacilityDAO facilityDAO;

    @Before
    public void setUp() throws Exception {
        facilityDAO = new CFacilityDAO();
    }

    @Test
    public void testGetFacility() throws Exception {
        IFacility fac = facilityDAO.getFacility(0);
        System.out.println(fac.getFacilityId() + " " + fac.getFacilityName() + " " + fac.getFacilityDescription());
        assertNotNull(fac);
    }

    @Test
    public void testAddFacility() throws Exception {
        IFacility fac = new CGeneralFacility(3, "AnotherFacility", "testing this", "Chicago", 300, 300);
        facilityDAO.addFacility(fac);
    }

    @Test
    public void testGetAllFacilities() throws Exception {
        List<IFacility> facs = new ArrayList<IFacility>();
        facs = facilityDAO.getAllFacilities();
        assertNotNull(facs);
        assertTrue(facs.size() > 0);
    }

    @Test
    public void testDeleteFacility() throws Exception {
        IFacility fac = new CGeneralFacility(3, null, null, null, 0, 0);
        facilityDAO.deleteFacility(fac);
    }

    @Test
    public void testUpdateFacility() throws Exception {
        // Empty
    }
}
