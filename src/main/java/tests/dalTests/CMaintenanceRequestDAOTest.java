package tests.dalTests;

import com.facility.system.dal.CMaintenanceRequestDAO;
import com.facility.system.dal.IMaintenanceRequestDAO;
import com.facility.system.model.facmaint.CGeneralMaintenance;
import com.facility.system.model.facmaint.CMaintenanceRequest;
import com.facility.system.model.facmaint.IMaintenanceRequest;
import com.facility.system.model.facoverview.CGeneralFacility;
import com.facility.system.model.utility.Time;
import com.facility.system.model.utility.TimeStamp;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 1/30/14.
 */
public class CMaintenanceRequestDAOTest {

    IMaintenanceRequestDAO maintReqDAO;

    @Before
    public void setUp() throws Exception {
        maintReqDAO = new CMaintenanceRequestDAO();
    }

    @Test
    public void testGetMaintenanceRequest() throws Exception {
        IMaintenanceRequest m = maintReqDAO.getMaintenanceRequest(0);
        assertNotNull(m);
        System.out.println(m.getMaintReqId() + " " + m.getFacilityRequested().getFacilityName() + " " +
                m.getMaintenance().getMaintenanceDescription() + " " + m.getIsDone());
    }

    @Test
    public void testAddMaintenanceRequest() throws Exception {
        IMaintenanceRequest m = new CMaintenanceRequest(2, new CGeneralFacility(0, null, null, null, 0, 0),
                new CGeneralMaintenance(0, 0, null, null, null), new TimeStamp(3, 1, 2014, new Time(0, 0, 0, 0)).toString(), false);
        maintReqDAO.addMaintenanceRequest(m);
    }

    @Test
    public void testGetAllMaintenanceRequest() throws Exception {
        List<IMaintenanceRequest> maints = maintReqDAO.getAllMaintenanceRequest(new CGeneralFacility(0, null, null, null, 0, 0));
        assertNotNull(maints);
        assertTrue(maints.size() > 0);
    }

    @Test
    public void testDeleteMaintenanceRequest() throws Exception {
        maintReqDAO.deleteMaintenanceRequest(new CMaintenanceRequest(2, null, null, null, false));
    }

    @Test
    public void testUpdateMaintenanceRequest() throws Exception {
        IMaintenanceRequest m = maintReqDAO.getMaintenanceRequest(0);
        IMaintenanceRequest m2 = new CMaintenanceRequest(m.getMaintReqId(), m.getFacilityRequested(), m.getMaintenance(), m.getTimeRequested(), m.getIsDone());
        m2.setIsDone(true);
        m2.setTimeRequested(new TimeStamp(9, 15, 1991, new Time(0, 0, 0, 0)).toString());
        maintReqDAO.updateMaintenanceRequest(m2);
        m2 = maintReqDAO.getMaintenanceRequest(0);
        assertTrue(m2.getIsDone());
        assertTrue(m2.getTimeRequested().toString().equals("1991-09-15 00:00:00"));
        maintReqDAO.updateMaintenanceRequest(m);
    }

    @Test
    public void testAddMaintenanceRequestToHistory() throws Exception   {
        IMaintenanceRequest m = maintReqDAO.getMaintenanceRequest(0);
        maintReqDAO.addMaintenanceRequestToHistory(m);
        assertTrue(m.getIsDone());
        m.setIsDone(false);
        maintReqDAO.updateMaintenanceRequest(m);
    }

    @Test
    public void testGetAllMaintenanceRequestsFromHistory() throws Exception {
        List<IMaintenanceRequest> m = new ArrayList<IMaintenanceRequest>();
        m = maintReqDAO.getAllMaintenanceRequestsFromHistory(new CGeneralFacility(0, "", "", "", 0, 0));
        assertTrue(m.size() > 0);
        for(IMaintenanceRequest q : m){
            assertTrue(q.getIsDone());
        }
    }
}
