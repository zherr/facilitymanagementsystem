package tests.dalTests;

import com.facility.system.dal.CInspectionDAO;
import com.facility.system.model.facoperation.CInspection;
import com.facility.system.model.facoperation.IInspection;
import com.facility.system.model.facoverview.CGeneralFacility;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 1/30/14.
 */
public class CInspectionDAOTest {

    CInspectionDAO inspectionDAO;

    @Before
    public void setUp() throws Exception {
        inspectionDAO = new CInspectionDAO();
    }

    @Test
    public void testGetInspection() throws Exception {
        IInspection inspection = inspectionDAO.getInspection(0);
        System.out.println(inspection.getInspectFacility().getFacilityName() + " " + inspection.getInspectionName());
        assertNotNull(inspection);
    }

    @Test
    public void testAddInspection() throws Exception {
        IInspection inspection = new CInspection(2, "Pest", new CGeneralFacility(0, "", "", "", 0, 0));
        inspectionDAO.addInspection(inspection);
    }

    @Test
    public void testGetAllInspections() throws Exception {
        List<IInspection> inspections = new ArrayList<IInspection>();
        inspections = inspectionDAO.getAllInspections(new CGeneralFacility(0, "", "", "", 0, 0));
        assertNotNull(inspections);
        assertTrue(inspections.size() > 0);
    }

    @Test
    public void testDeleteFacility() throws Exception {
        inspectionDAO.deleteInspection(new CInspection(2, "", null));
    }
}
