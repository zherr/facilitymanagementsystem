package tests.dalTests;

import com.facility.system.dal.CMaintenanceDAO;
import com.facility.system.model.facmaint.CGeneralMaintenance;
import com.facility.system.model.facmaint.IMaintenance;
import com.facility.system.model.facoverview.CGeneralFacility;
import com.facility.system.model.utility.Time;
import com.facility.system.model.utility.TimeStamp;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 1/30/14.
 */
public class CMaintenanceDAOTest {

    CMaintenanceDAO maintenanceDAO;

    @Before
    public void setUp() throws Exception {
        maintenanceDAO = new CMaintenanceDAO();
    }

    @Test
    public void testGetMaintenance() throws Exception {
        IMaintenance maint = maintenanceDAO.getMaintenance(0);
        assertNotNull(maint);
        System.out.println(maint.getCostOfMaintenance() + " " + maint.getMaintenanceDescription() + " " + maint.getMaintenanceStart());
    }

    @Test
    public void testAddMaintenance() throws Exception {
        TimeStamp t = new TimeStamp(9, 15, 2015, new Time(0, 0, 0, 0));
        TimeStamp t2 = new TimeStamp(10, 15, 2015, new Time(0, 0, 0, 0));
        String tString = t.toString();
        String t2String = t2.toString();
        IMaintenance maint = new CGeneralMaintenance(2, 5000, "fixing cubicles", tString, t2String);
        maintenanceDAO.addMaintenance(maint);
    }

    @Test
    public void testListAllMaintenance() throws Exception {
        List<IMaintenance> maints = maintenanceDAO.listAllMaintenance(new CGeneralFacility(0, null, null, null, 0, 0));
        assertNotNull(maints);
        assertTrue(maints.size() > 0);
    }

    @Test
    public void testDeleteMaintenance() throws Exception {
        maintenanceDAO.deleteMaintenance(new CGeneralMaintenance(2, 0, null, null, null));
    }

    @Test
    public void testUpdateMaintenance() throws Exception {
        // Empty
    }
}
