package tests.dalTests;

import com.facility.system.dal.CReservationDAO;
import com.facility.system.model.facoperation.CReservation;
import com.facility.system.model.facoperation.IReservation;
import com.facility.system.model.facoverview.CGeneralFacility;
import com.facility.system.model.user.CRenter;
import com.facility.system.model.utility.Time;
import com.facility.system.model.utility.TimeStamp;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 1/31/14.
 */
public class CReservationDAOTest {

    CReservationDAO reservationDAO;

    @Before
    public void setUp() throws Exception {
        reservationDAO = new CReservationDAO();
    }

    @Test
    public void testGetReservation() throws Exception {
        IReservation r = reservationDAO.getReservation(0);
        assertNotNull(r);
        assertNotNull(r.getRenter());
        System.out.println(r.getTitle() + " " + r.getNumOfParticipants() + " " + r.getStart() + " " + " " + r.getEnd());
    }

    @Test
    public void testAddReservation() throws Exception {
        IReservation r0 = new CReservation(2, new CGeneralFacility(0, "", "", "", 0, 0), new TimeStamp(9, 15, 1991,
                new Time(0, 0, 0 ,0)).toString(), new TimeStamp(9, 15, 1991, new Time(0, 0, 0 ,0)).toString(), "test", 5, new CRenter(1234567890, "renter"));
        reservationDAO.addReservation(r0);
    }

    @Test
    public void testGetAllReservations() throws Exception {
        List<IReservation> rs = new ArrayList<IReservation>();
        rs = reservationDAO.getAllReservations(new CGeneralFacility(0, "" ,"", "", 0, 0));
        assertTrue(rs.size() > 0);
    }

    @Test
    public void testDeleteReservation() throws Exception {
        IReservation r1 = new CReservation(2, new CGeneralFacility(0, "", "", "", 0, 0), new TimeStamp(9, 15, 1991,
                new Time(0, 0, 0 ,0)).toString(), new TimeStamp(9, 15, 1991, new Time(0, 0, 0 ,0)).toString(), "test", 5, new CRenter(1234567890, "renter"));
        reservationDAO.deleteReservation(r1);
    }

    @Test
    public void testUpdateReservation() throws Exception {
        // Empty
    }

    @Test
    public void testDeleteAllReservations() throws Exception {
        // Empty
    }
}
