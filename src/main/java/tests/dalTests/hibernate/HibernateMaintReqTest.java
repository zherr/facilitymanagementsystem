package tests.dalTests.hibernate;

import com.facility.system.dal.hibernate.HibernateMaintenanceRequestDAO;
import com.facility.system.model.facmaint.CGeneralMaintenance;
import com.facility.system.model.facmaint.CMaintenanceRequest;
import com.facility.system.model.facmaint.IMaintenanceRequest;
import com.facility.system.model.facoverview.COwnedFacility;
import com.facility.system.model.user.COwner;
import com.facility.system.model.utility.Time;
import com.facility.system.model.utility.TimeStamp;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by zherr on 2/28/14.
 */
public class HibernateMaintReqTest {

    HibernateMaintenanceRequestDAO hibernateMaintenanceRequestDAO;
    CMaintenanceRequest maintReq;
    COwnedFacility oFac;
    CGeneralMaintenance gMaint;
    COwner owner;
    TimeStamp t;

    @Before
    public void setUp() {
        hibernateMaintenanceRequestDAO = new HibernateMaintenanceRequestDAO();
        t = new TimeStamp(9, 15, 1991, new Time(0, 0, 0, 0));
        owner = new COwner(123456789, "testOwner");
        oFac = new COwnedFacility(999, "testFac", "testDescr", "Chicago", 50, 50, owner);
        gMaint = new CGeneralMaintenance(99, 500, "testDescr", t.toString(), t.toString());
        maintReq = new CMaintenanceRequest(99, oFac, gMaint, t.toString(), false);
    }

    @Test
    public void testGetMaintenanceRequest() {
        CMaintenanceRequest maint1 = (CMaintenanceRequest) hibernateMaintenanceRequestDAO.getMaintenanceRequest(0);
        assertNotNull(maint1);
        assertNotNull(maint1.getMaintenance());
        assertNotNull(maint1.getFacilityRequested());
        System.out.println("Maintenance Request for facility: " + maint1.getFacilityRequested().getFacilityName());
        System.out.println("Maintenance Request maintenance: " + maint1.getMaintenance().getMaintenanceDescription());
    }

    @Test
    public void testAddMaintenanceRequest() {
        hibernateMaintenanceRequestDAO.addMaintenanceRequest(maintReq);
        CMaintenanceRequest m = (CMaintenanceRequest) hibernateMaintenanceRequestDAO.getMaintenanceRequest(99);
        assertNotNull(m);
        assertNotNull(m.getFacilityRequested());
        assertNotNull(m.getMaintenance());
        System.out.println("Maintenance Request for facility: " + m.getFacilityRequested().getFacilityName());
        System.out.println("Maintenance Request maintenance: " + m.getMaintenance().getMaintenanceDescription());
    }

    @Test
    public void testDeleteMaintenanceRequest() {
        hibernateMaintenanceRequestDAO.deleteMaintenanceRequest(maintReq);
        CMaintenanceRequest m = (CMaintenanceRequest) hibernateMaintenanceRequestDAO.getMaintenanceRequest(99);
        assertNull(m);
    }

    @Test
    public void testGetMaintReqsByFacility() {
        COwnedFacility realFac = new COwnedFacility(0, "", "", "", 0, 0, new COwner(123456789, "testOwner"));
        List<IMaintenanceRequest> maintReqs = hibernateMaintenanceRequestDAO.getAllMaintenanceRequest(realFac);
        assertNotNull(maintReqs);
        assertTrue(maintReqs.size() > 0);
        for(IMaintenanceRequest m : maintReqs) {
            System.out.println("Maint Req: " + m.getMaintenance().getMaintenanceDescription() + " " + m.getFacilityRequested().getFacilityName());
        }
    }

    @Test
    public void testGetMaintReqsHistory() {
        COwnedFacility realFac = new COwnedFacility(0, "", "", "", 0, 0, new COwner(123456789, "testOwner"));
        List<IMaintenanceRequest> maintReqs = hibernateMaintenanceRequestDAO.getAllMaintenanceRequestsFromHistory(realFac);
        assertNotNull(maintReqs);
        assertTrue(maintReqs.size() > 0);
        for(IMaintenanceRequest m : maintReqs) {
            System.out.println("Maint Req: " + m.getMaintenance().getMaintenanceDescription() + " " + m.getFacilityRequested().getFacilityName());
        }
    }

    @Test
    public void testGetCurrentMaintReqs() {
        COwnedFacility realFac = new COwnedFacility(0, "", "", "", 0, 0, new COwner(123456789, "testOwner"));
        List<IMaintenanceRequest> maintReqs = hibernateMaintenanceRequestDAO.getAllMaintenanceRequestsCurrent(realFac);
        assertNotNull(maintReqs);
        assertTrue(maintReqs.size() > 0);
        for(IMaintenanceRequest m : maintReqs) {
            System.out.println("Maint Req: " + m.getMaintenance().getMaintenanceDescription() + " " + m.getFacilityRequested().getFacilityName());
        }
    }

    @Test
    public void testUpdateMaintenanceRequest() {
        hibernateMaintenanceRequestDAO.addMaintenanceRequest(maintReq);
        maintReq.setIsDone(true);
        oFac.setFacilityId(1);
        maintReq.setFacilityRequested(oFac);
        hibernateMaintenanceRequestDAO.updateMaintenanceRequest(maintReq);
        CMaintenanceRequest nMaintReq = (CMaintenanceRequest) hibernateMaintenanceRequestDAO.getMaintenanceRequest(99);
        assertNotNull(nMaintReq);
        assertTrue(nMaintReq.getIsDone());
        assertTrue(nMaintReq.getFacilityRequested().getFacilityId() == 1);

        hibernateMaintenanceRequestDAO.deleteMaintenanceRequest(nMaintReq);
    }

    @Test
    public void testAddHist() {
        hibernateMaintenanceRequestDAO.addMaintenanceRequest(maintReq);
        hibernateMaintenanceRequestDAO.addMaintenanceRequestToHistory(maintReq);
        CMaintenanceRequest m = (CMaintenanceRequest) hibernateMaintenanceRequestDAO.getMaintenanceRequest(99);
        assertNotNull(m);
        assertTrue(m.getIsDone());

        hibernateMaintenanceRequestDAO.deleteMaintenanceRequest(m);
    }
}
