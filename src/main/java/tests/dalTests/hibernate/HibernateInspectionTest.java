package tests.dalTests.hibernate;

import com.facility.system.dal.hibernate.HibernateInspectionDAO;
import com.facility.system.model.facoperation.CInspection;
import com.facility.system.model.facoperation.IInspection;
import com.facility.system.model.facoverview.COwnedFacility;
import com.facility.system.model.user.COwner;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 2/27/14.
 */
public class HibernateInspectionTest {

    HibernateInspectionDAO hibernateInspectionDAO;
    CInspection insp;

    @Before
    public void setUp() {
        hibernateInspectionDAO = new HibernateInspectionDAO();
        insp = new CInspection(99, "testInspection", new COwnedFacility(0, "", "", "", 0, 0, new COwner(123456789, "")));
    }

    @Test
    public void testGetInspection() {
        CInspection inspection = (CInspection) hibernateInspectionDAO.getInspection(0);
        assertNotNull(inspection);
        assertNotNull(inspection.getInspectFacility());
        assertNotNull(inspection.getInspectFacility().getFacilityName());
        System.out.println("Inspection: " + inspection.getInspectId() + " " + inspection.getInspectionName());
        System.out.println(inspection.getInspectFacility().getFacilityName());
    }

    @Test
    public void testDeleteInspection() {
        hibernateInspectionDAO.deleteInspection(insp);
        assertNull((CInspection) hibernateInspectionDAO.getInspection(99));
    }

    @Test
    public void testAddInspection() {
        hibernateInspectionDAO.addInspection(insp);
        CInspection inspection = (CInspection) hibernateInspectionDAO.getInspection(99);
        assertNotNull(inspection);
        assertNotNull(inspection.getInspectionName());
        assertNotNull(inspection.getInspectFacility());
        assertNotNull(inspection.getInspectFacility().getFacilityName());
    }

    @Test
    public void testGetAllInspections() {
        List<IInspection> inspectionList = hibernateInspectionDAO.getAllInspections(new COwnedFacility(0, "", "", "", 0, 0,
                new COwner(123456789, "")));
        assertNotNull(inspectionList);
        assertTrue(inspectionList.size() > 0);
    }
}
