package tests.dalTests.hibernate;

import com.facility.system.dal.hibernate.HibernateMaintenanceDAO;
import com.facility.system.model.facmaint.CGeneralMaintenance;
import com.facility.system.model.facmaint.IMaintenance;
import com.facility.system.model.facoverview.COwnedFacility;
import com.facility.system.model.user.COwner;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by zherr on 2/27/14.
 */
public class HibernateMaintenanceTest {

    HibernateMaintenanceDAO hibernateMaintenanceDAO;
    CGeneralMaintenance maint;
    COwner rOwner;
    COwnedFacility oFac;

    @Before
    public void setUp() {
        hibernateMaintenanceDAO = new HibernateMaintenanceDAO();
        maint = new CGeneralMaintenance(99, 500, "testmaindescr", "2000-09-15 05:05:00", "2001-09-15 05-05-00");
        rOwner = new COwner(123456789, "testOwner");
        oFac = new COwnedFacility(0, "", "", "", 0, 0, rOwner);
    }

    @Test
    public void testGetMaintenance() {
        CGeneralMaintenance maintenance = (CGeneralMaintenance) hibernateMaintenanceDAO.getMaintenance(0);
        assertNotNull(maintenance);
        assertNotNull(maintenance.getMaintenanceStart());
        System.out.println(maintenance.getMaintenanceStart() + " - " + maintenance.getMaintenanceEnd());
    }

    @Test
    public void testDeleteMaintenance() {
        hibernateMaintenanceDAO.deleteMaintenance(maint);
        assertNull(hibernateMaintenanceDAO.getMaintenance(99));
    }

    @Test
    public void testAddMaintenance() {
        hibernateMaintenanceDAO.addMaintenance(maint);
        CGeneralMaintenance maintenance = (CGeneralMaintenance) hibernateMaintenanceDAO.getMaintenance(99);
        assertNotNull(maintenance);
        System.out.println("Maintenance: " + maintenance.getMaintenanceDescription() + " " + maintenance.getCostOfMaintenance() + " "
                            + maintenance.getMaintenanceStart());
    }

    @Test
    public void testGetAllMaints() {
        List<IMaintenance> maints = hibernateMaintenanceDAO.listAllMaintenance(oFac);
        assertNotNull(maints);
        assertTrue(maints.size() > 0);
        for(IMaintenance m : maints) {
            System.out.println("Maint: " + m.getMaintId() + " " + m.getMaintenanceDescription());
        }
    }
}
