package tests.dalTests.hibernate;

import com.facility.system.dal.hibernate.HibernateOwnerDAO;
import com.facility.system.model.user.COwner;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by zherr on 2/27/14.
 */
public class HibernateOwnerTest {

    HibernateOwnerDAO hibernateOwnerDAO;
    COwner owner;

    @Before
    public void setUp() {
        hibernateOwnerDAO = new HibernateOwnerDAO();
        owner = new COwner(555555555, "testOwner");
    }

    @Test
    public void testGetOwner() {
        COwner owner = (COwner) hibernateOwnerDAO.getOwner(123456789);
        assertNotNull(owner);
        System.out.println("Owner info: " + owner.getName() + " " + owner.getSsn());
    }

    @Test
    public void testDeleteOwner() {
        hibernateOwnerDAO.deleteOwner(owner);
        assertNull((COwner) hibernateOwnerDAO.getOwner(555555555));
    }

    @Test
    public void testAddOwner() {
        hibernateOwnerDAO.addOwner(owner);
        assertNotNull((COwner) hibernateOwnerDAO.getOwner(555555555));
    }

}
