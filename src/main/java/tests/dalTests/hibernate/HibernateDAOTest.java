package tests.dalTests.hibernate;

import com.facility.system.dal.hibernate.HibernateMySQLHelper;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by zherr on 2/27/14.
 */
public class HibernateDAOTest {

    SessionFactory sessionFactory;

    @Before
    public void setUp() {
        sessionFactory = HibernateMySQLHelper.getSessionFactory();
    }

    @Test
    public void testGetSessionFactory() {
        assertNotNull(sessionFactory);
    }

}
