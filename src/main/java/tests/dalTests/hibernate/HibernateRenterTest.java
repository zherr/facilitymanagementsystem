package tests.dalTests.hibernate;

import com.facility.system.dal.hibernate.HibernateRenterDAO;
import com.facility.system.model.user.CRenter;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by zherr on 2/27/14.
 */
public class HibernateRenterTest {

    HibernateRenterDAO hibernateRenterDAO;
    CRenter renter;

    @Before
    public void setUp() {
        hibernateRenterDAO = new HibernateRenterDAO();
        renter = new CRenter(555555555, "testRenter");
    }

    @Test
    public void testGetRenter() {
        CRenter renterr = (CRenter) hibernateRenterDAO.getRenter(112233445);
        assertNotNull(renterr);
        System.out.println("Renter info: " + renterr.getName() + " " + renterr.getSsn());
    }

    @Test
    public void testDeleteRenter() {
        hibernateRenterDAO.deleteRenter(renter);
        assertNull((CRenter) hibernateRenterDAO.getRenter(555555555));
    }

    @Test
    public void testAddOwner() {
        hibernateRenterDAO.addRenter(renter);
        assertNotNull((CRenter) hibernateRenterDAO.getRenter(555555555));
    }
}
