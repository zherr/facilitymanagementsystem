package tests.dalTests.hibernate;

import com.facility.system.dal.hibernate.HibernateReservationDAO;
import com.facility.system.model.facoperation.CReservation;
import com.facility.system.model.facoperation.IReservation;
import com.facility.system.model.facoverview.COwnedFacility;
import com.facility.system.model.user.COwner;
import com.facility.system.model.user.CRenter;
import com.facility.system.model.utility.Time;
import com.facility.system.model.utility.TimeStamp;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by zherr on 2/28/14.
 */
public class HibernateReservationTest {

    HibernateReservationDAO hibernateReservationDAO;
    CReservation reservation;
    COwnedFacility fac;
    CRenter renter;
    TimeStamp t;

    @Before
    public void setUp() {
        hibernateReservationDAO = new HibernateReservationDAO();
        fac = new COwnedFacility(1, "", "", "", 0, 0, new COwner(123456789, ""));
        renter = new CRenter(566778899, "testRenter");
        t = new TimeStamp(9, 15, 1991, new Time(0, 0, 0, 0));
        reservation = new CReservation(99, fac, t.toString(), t.toString(), "testReservation", 20, renter);
    }

    @Test
    public void testGetReservation() {
        CReservation r = (CReservation) hibernateReservationDAO.getReservation(1);
        assertNotNull(r);
        assertNotNull(r.getFacToReserve());
        assertNotNull(r.getRenter());
    }

    @Test
    public void testAddResevation() {
        hibernateReservationDAO.addReservation(reservation);
        CReservation temp = (CReservation) hibernateReservationDAO.getReservation(99);
        assertNotNull(temp);
        assertNotNull(temp.getFacToReserve());
        assertNotNull(temp.getRenter());
    }

    @Test
    public void testDeletereservation() {
        hibernateReservationDAO.deleteReservation(reservation);
        assertNull(hibernateReservationDAO.getReservation(99));
    }

    @Test
    public void testGetAllReservationsByFacility() {
        List<IReservation> reservations = hibernateReservationDAO.getAllReservations(fac);
        assertNotNull(reservations);
        assertTrue(reservations.size() > 0);
        for(IReservation res : reservations) {
            System.out.println("Reservations: " + res.getReserveId() + " " + res.getFacToReserve().getFacilityName() +
                    " " + res.getRenter().getName());
        }
    }

    @Test
    public void testDeleteAllReservationByFacility() {
        List<IReservation> reservations = hibernateReservationDAO.getAllReservations(new COwnedFacility(0, "", "", "", 0, 0, new COwner(123456789, "test")));
        hibernateReservationDAO.deleteAllReservations(new COwnedFacility(0, "", "", "", 0, 0, new COwner(123456789, "test")));
        List<IReservation> deleted = hibernateReservationDAO.getAllReservations(new COwnedFacility(0, "", "", "", 0, 0, new COwner(123456789, "test")));
        assertNull(deleted);
        for(IReservation r : reservations) {
            hibernateReservationDAO.addReservation(r);
        }
    }
}
