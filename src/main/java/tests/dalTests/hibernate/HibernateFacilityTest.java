package tests.dalTests.hibernate;

import com.facility.system.dal.hibernate.HibernateFacilityDAO;
import com.facility.system.model.facoverview.COwnedFacility;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.user.COwner;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 2/27/14.
 */
public class HibernateFacilityTest {

    HibernateFacilityDAO hibernateFacilityDAO;
    COwnedFacility fac;

    @Before
    public void setUp() {
        hibernateFacilityDAO = new HibernateFacilityDAO();
        fac = new COwnedFacility(99, "testFacility", "descr", "Chicago", 50, 50, new COwner(123456789, "testOwner"));
    }

    @Test
    public void testAddFacility() {
        hibernateFacilityDAO.addFacility(fac);
        COwnedFacility oFac = (COwnedFacility) hibernateFacilityDAO.getFacility(99);
        assertNotNull(oFac);
        assertNotNull(oFac.getOwner());
        System.out.println(oFac.getOwner().getSsn() + " " + oFac.getOwner().getName());
    }

    @Test
    public void testDeleteFacility() {
        hibernateFacilityDAO.deleteFacility(fac);
        assertNull(hibernateFacilityDAO.getFacility(99));
    }

    @Test
    public void testGetFacility() {
        COwnedFacility fac = (COwnedFacility) hibernateFacilityDAO.getFacility(0);
        assertNotNull(fac);
        System.out.println("Facility: " + fac.getFacilityName() + " " + fac.getFacilityLocation());
        assertNotNull(fac.getOwner());
        System.out.println("Facility's renter: " + fac.getOwner().getSsn());
    }

    @Test
    public void testGetAllFacilities() {
        List<IFacility> facs = hibernateFacilityDAO.getAllFacilities();
        assertNotNull(facs);
        assertTrue(facs.size() > 0);
        System.out.println("Facilities:\n");
        for(IFacility oFac : facs) {
            System.out.println("f: " + oFac.getFacilityId() + oFac.getFacilityName() + ((COwnedFacility)oFac).getOwner().getName());
        }
    }

    @Test
    public void testUpdateFacility() {
        hibernateFacilityDAO.addFacility(fac);
        fac.setFacilityName("TEST");
        fac.setOwner(new COwner(987654321, "TESTOWNER"));
        hibernateFacilityDAO.updateFacility(fac);
        fac = (COwnedFacility) hibernateFacilityDAO.getFacility(fac.getFacilityId());
        assertTrue(fac.getFacilityName().equals("TEST"));
        assertTrue(fac.getOwner().getSsn() == 987654321);
        fac.setFacilityName("testFacility");
        fac.setOwner(new COwner(123456789, "testOwner"));
        hibernateFacilityDAO.updateFacility(fac);
    }
}
