package tests.userservicesTests;

import com.facility.system.dal.CReservationDAO;
import com.facility.system.model.facoperation.CFacilityReserver;
import com.facility.system.model.facoperation.CReservation;
import com.facility.system.model.facoperation.IReservation;
import com.facility.system.model.user.CRenter;
import com.facility.system.model.userservices.CRenterService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertTrue;

/**
 * Created by Zach on 2/22/14.
 */
public class CRenterServiceTest {

    CRenterService renterService;

    @Before
    public void setUp() throws Exception {
        renterService = new CRenterService(new CFacilityReserver(new CReservationDAO(), new ArrayList<IReservation>()));
    }

    @Test
    public void testMakeReservation() throws Exception {
        // TODO
        assertTrue(false);
    }

    @Test
    public void testRemoveReservation() throws Exception {
        // TODO
        assertTrue(false);
    }

    @Test
    public void testCheckIfReservedByRenter() throws Exception {
        CRenter renter = new CRenter(0, "renter");
        CReservation res = new CReservation(0, null, null, null, "", 0, renter);
        assertTrue(renterService.checkIfReservedByRenter(res, renter));
    }

    @Test
    public void testSetFacilityReserver() throws Exception {
        renterService.setFacilityReserver(new CFacilityReserver(null, null));
    }
}
