package tests.facoverviewTests;

import com.facility.system.model.facoverview.CGeneralFacility;
import com.facility.system.model.facoverview.IFacility;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 1/27/14.
 */
public class CGeneralFacilityTest {

    IFacility facility;

    @org.junit.Before
    public void setUp() throws Exception {
        facility = new CGeneralFacility(0, "test", "a testing facility", "Chicago", 500, 500);
    }

    @Test
    public void testFacility(){
        assertTrue(facility.getFacilityId() == 0);
        assertTrue(facility.getFacilityName().equals("test"));
        assertTrue(facility.getFacilityDescription().equals("a testing facility"));
        assertTrue(facility.getFacilityLocation().equals("Chicago"));
        assertTrue(facility.getAvailableCapacity() == 500);
        assertTrue(facility.getTotalCapacity() == 500);
    }
}
