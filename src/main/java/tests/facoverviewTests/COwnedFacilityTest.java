package tests.facoverviewTests;

import com.facility.system.model.facoverview.COwnedFacility;
import com.facility.system.model.user.COwner;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Zach on 2/22/14.
 */
public class COwnedFacilityTest {

    COwnedFacility fac;

    @Before
    public void setUp() throws Exception {
        fac = new COwnedFacility(0, "test", "test descr", "chicago", 55, 55, null);
    }

    @Test
    public void testSetGetOwner() throws Exception {
        fac.setOwner(new COwner(0, "Owner"));
        assertNotNull(fac.getOwner());
    }
}
