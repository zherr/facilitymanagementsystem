package tests.facoverviewTests;

import com.facility.system.dal.CFacilityDAO;
import com.facility.system.dal.COwnedFacilityDAO;
import com.facility.system.model.facoverview.*;
import com.facility.system.model.user.COwner;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by zherr on 1/27/14.
 */
public class CFacilityOverseerTest {

    IFacilityOverseer facilityOverseer;

    @Before
    public void setUp() throws Exception {
        facilityOverseer = new CFacilityOverseer(new COwnedFacilityDAO());
    }

    @Test
    public void testListFacilities() throws Exception {
        assertTrue(facilityOverseer.listFacilities().size() > 0);
    }

    @Test
    public void testGetFacilityInformation() throws Exception {
        CFacilityDAO facDAO = new CFacilityDAO();
        IFacility fac0 = new CGeneralFacility();
        fac0 = facDAO.getFacility(0);
        assertFalse(facilityOverseer.getFacilityInformation(fac0).equals(""));
    }

    @Test
    public void testRequestAvailableCapacity() throws Exception {
        CFacilityDAO facDAO = new CFacilityDAO();
        IFacility fac0 = new CGeneralFacility();
        fac0 = facDAO.getFacility(0);
        assertTrue(facilityOverseer.requestAvailableCapacity(fac0) > 0);
    }

    @Test
    public void testAddNewFacility() throws Exception {
        CFacilityDAO facDAO = new CFacilityDAO();
        IFacility fac0 = new CGeneralFacility();
        fac0 = new CGeneralFacility(55, "test", "", "", 0, 0);
        facilityOverseer.addNewFacility(fac0);
        fac0 = facDAO.getFacility(55);
        assertTrue(fac0.getFacilityName().equals("test"));
        facDAO.deleteFacility(fac0);
    }

    @Test
    public void testAddFacilityDetail() throws Exception {
        COwnedFacilityDAO facDAO = new COwnedFacilityDAO();
        COwnedFacility fac0 = new COwnedFacility();
        fac0 = new COwnedFacility(55, "test", "", "", 0, 0, new COwner(123456789, "testOwner"));
        facilityOverseer.addNewFacility(fac0);
        facilityOverseer.addFacilityDetail(55, "test2", "", "", 0, 0, new COwner(987654321, "testOwner2"));
        fac0 = (COwnedFacility) facDAO.getFacility(55);
        assertTrue(fac0.getFacilityName().equals("test2"));
        assertTrue(fac0.getOwner().getSsn() == 987654321);
        facDAO.deleteFacility(fac0);
    }

    @Test
    public void testRemoveFacility() throws Exception {
        CFacilityDAO facDAO = new CFacilityDAO();
        IFacility fac0 = new CGeneralFacility();
        fac0 = new CGeneralFacility(55, "test", "", "", 0, 0);
        facilityOverseer.addNewFacility(fac0);
        fac0 = facDAO.getFacility(55);
        facDAO.deleteFacility(fac0);
        assertTrue(facDAO.getFacility(55).getFacilityDescription() == null);
    }
}
