package tests.facmaintTests;

import com.facility.system.model.facmaint.CMaintRequestFactory;
import com.facility.system.model.facmaint.IMaintenanceRequestFactory;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by zherr on 1/27/14.
 */
public class CMaintRequestFactoryTest {

    IMaintenanceRequestFactory factory;

    @Before
    public void setUp() throws Exception {
        factory = new CMaintRequestFactory();
    }

    @Test
    public void testMakeMaintenanceRequest() throws Exception {
        assertNotNull(factory.createMaintenanceRequest(0, null, null, null, false));
    }
}
