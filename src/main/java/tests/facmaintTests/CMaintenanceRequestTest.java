package tests.facmaintTests;

import com.facility.system.model.facmaint.CGeneralMaintenance;
import com.facility.system.model.facmaint.CMaintenanceRequest;
import com.facility.system.model.facmaint.IMaintenanceRequest;
import com.facility.system.model.facoverview.CGeneralFacility;
import com.facility.system.model.utility.Time;
import com.facility.system.model.utility.TimeStamp;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 1/27/14.
 */
public class CMaintenanceRequestTest {

    IMaintenanceRequest maintenanceRequest;

    @Before
    public void setUp() throws Exception {
        maintenanceRequest = new CMaintenanceRequest(0, new CGeneralFacility(0, "", "", "", 0, 0), new CGeneralMaintenance(0, 0, "", null, null), null, false);
    }

    @Test
    public void testGetFacilityRequested() throws Exception {
        assertNotNull(maintenanceRequest.getFacilityRequested());
    }

    @Test
    public void testGetMaintenance() throws Exception {
        assertNotNull(maintenanceRequest.getMaintenance());
    }

    @Test
    public void testGetTimeRequested() throws Exception {
        assertNull(maintenanceRequest.getTimeRequested());
    }

    @Test
    public void testSetFacilityRequested() throws Exception {
        maintenanceRequest.setFacilityRequested(null);
        assertNull(maintenanceRequest.getFacilityRequested());
    }

    @Test
    public void testSetMaintenance() throws Exception {
        maintenanceRequest.setMaintenance(null);
        assertNull(maintenanceRequest.getMaintenance());
    }

    @Test
    public void testSetTimeRequested() throws Exception {
        maintenanceRequest.setTimeRequested(new TimeStamp(0, 0, 0, new Time(0, 0, 0, 0)).toString());
        assertNotNull(maintenanceRequest.getTimeRequested());
    }

    @Test
    public void testGetSetMaintReqId() throws Exception {
        assertTrue(maintenanceRequest.getMaintReqId() == 0);
        maintenanceRequest.setMaintReqId(1);
        assertTrue(maintenanceRequest.getMaintReqId() == 1);
    }

    @Test
    public void testGetSetDone() throws Exception {
        assertFalse(maintenanceRequest.getIsDone());
        maintenanceRequest.setIsDone(true);
        assertTrue(maintenanceRequest.getIsDone());
    }
}
