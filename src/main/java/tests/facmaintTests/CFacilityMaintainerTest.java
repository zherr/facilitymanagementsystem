package tests.facmaintTests;

import com.facility.system.dal.CMaintenanceDAO;
import com.facility.system.dal.CMaintenanceRequestDAO;
import com.facility.system.dal.IMaintenanceRequestDAO;
import com.facility.system.model.facmaint.*;
import com.facility.system.model.facoverview.CGeneralFacility;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.utility.Time;
import com.facility.system.model.utility.TimeStamp;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * Created by zherr on 1/27/14.
 */
public class CFacilityMaintainerTest {

    IFacilityMaintainer facilityMaintainer;
    IMaintenanceRequestDAO maintReqDAO;

    @Before
    public void setUp() throws Exception {
        facilityMaintainer = new CFacilityMaintainer();
        facilityMaintainer.setMaintenanceRequestDAO(new CMaintenanceRequestDAO());
        facilityMaintainer.setMaintenanceDAO(new CMaintenanceDAO());
        CGeneralMaintScheduler scheduler = new CGeneralMaintScheduler(new ArrayList<IMaintenanceRequest>(),
                new ArrayList<IMaintenanceRequest>(), new CMaintenanceRequestDAO());
        facilityMaintainer.setMaintenanceScheduler(scheduler);
        maintReqDAO = new CMaintenanceRequestDAO();
    }

    @Test
    public void testMakeFacilityMaintRequest() throws Exception {
        IMaintenance m = new CGeneralMaintenance(0, 500, "test", new TimeStamp(9 ,15, 1991, new Time(1, 0, 0, 0)).toString(), new TimeStamp(8, 15,1991, new Time(1, 0, 0, 0)).toString());
        IFacility f = new CGeneralFacility(0, "", "", "", 0, 0);
        IMaintenanceRequest mq = new CMaintenanceRequest(55, f, m, new TimeStamp(9 ,15, 1991, new Time(1, 0, 0, 0)).toString(), false);
        facilityMaintainer.makeFacilityMaintRequest(mq);
        mq = maintReqDAO.getMaintenanceRequest(55);
        assertNotNull(mq);
        maintReqDAO.deleteMaintenanceRequest(mq);
    }

    @Test
    public void testScheduleMaintenance() throws Exception {
        IMaintenance m = new CGeneralMaintenance(0, 500, "test", new TimeStamp(9 ,15, 1991, new Time(1, 0, 0, 0)).toString(), new TimeStamp(8, 15,1991, new Time(1, 0, 0, 0)).toString());
        IFacility f = new CGeneralFacility(0, "", "", "", 0, 0);
        IMaintenanceRequest mq = new CMaintenanceRequest(55, f, m, new TimeStamp(9 ,15, 1991, new Time(1, 0, 0, 0)).toString(), false);
        facilityMaintainer.makeFacilityMaintRequest(mq);
        facilityMaintainer.scheduleMaintenance(mq);
        assertNotNull(maintReqDAO.getMaintenanceRequest(55));
        mq = maintReqDAO.getMaintenanceRequest(55);
        assertTrue(mq.getIsDone());
        maintReqDAO.deleteMaintenanceRequest(mq);
    }

    @Test
    public void testCalcMaintenanceCostForFacility() throws Exception {
        IFacility f = new CGeneralFacility(0, "", "", "", 0, 0);
        float cost = facilityMaintainer.calcMaintenanceCostForFacility(f);
        System.err.println(cost);
        assertTrue(cost != 0);
    }

    @Test
    public void testCalcDownTimeForFacility() throws Exception {
        IFacility f = new CGeneralFacility(0, "", "", "", 0, 0);
        int time = facilityMaintainer.calcDownTimeForFacility(f);
        System.err.println(time);
        assertTrue(time != 0);
    }

    @Test
    public void testListMaintRequest() throws Exception {
        IFacility f = new CGeneralFacility(0, "", "", "", 0, 0);
        assertNotNull(facilityMaintainer.listMaintRequest(f));
    }

    @Test
    public void testListMaintenance() throws Exception {
        IFacility f = new CGeneralFacility(0, "", "", "", 0, 0);
        assertNotNull(facilityMaintainer.listMaintenance(f));
        assertTrue(facilityMaintainer.listMaintenance(f).size() > 0);
    }

    @Test
    public void testListFacilityProblems() throws Exception {
        IFacility f = new CGeneralFacility(0, "", "", "", 0, 0);
        assertNotNull(facilityMaintainer.listFacilityProblems(f));
        assertTrue(facilityMaintainer.listFacilityProblems(f).size() > 0);
    }
}
