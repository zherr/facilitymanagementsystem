package tests.facmaintTests;

import com.facility.system.dal.CMaintenanceRequestDAO;
import com.facility.system.dal.IMaintenanceRequestDAO;
import com.facility.system.model.facmaint.*;
import com.facility.system.model.facoverview.CGeneralFacility;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.utility.Time;
import com.facility.system.model.utility.TimeStamp;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * Created by zherr on 1/27/14.
 */
public class CGeneralMaintSchedulerTest {

    IMaintenanceScheduler maintSched;
    IMaintenanceRequestDAO maintDAO;

    @Before
    public void setUp() throws Exception {
        maintSched = new CGeneralMaintScheduler();
        maintSched.setMaintenanceRequestQueue(new ArrayList<IMaintenanceRequest>());
        maintSched.setMaintenanceRequestHistory(new ArrayList<IMaintenanceRequest>());
        maintSched.setMaintenanceRequestDAO(new CMaintenanceRequestDAO());
        maintDAO = new CMaintenanceRequestDAO();
    }

    @Test
    public void testScheduleMaintenance() throws Exception {
        IMaintenance m = new CGeneralMaintenance(0, 500, "test", new TimeStamp(9 ,15, 1991, new Time(1, 0, 0, 0)).toString(), new TimeStamp(8, 15,1991, new Time(1, 0, 0, 0)).toString());
        IFacility f = new CGeneralFacility(0, "", "", "", 0, 0);
        IMaintenanceRequest mq = new CMaintenanceRequest(55, f, m, new TimeStamp(9 ,15, 1991, new Time(1, 0, 0, 0)).toString(), false);
        maintSched.scheduleMaintenance(mq);
        assertNotNull(maintDAO.getMaintenanceRequest(55));
        assertTrue(maintSched.getMaintenanceRequestQueue().size() > 0);
        maintDAO.deleteMaintenanceRequest(mq);
    }

    @Test
    public void testDoMaintenance() throws Exception {
        IMaintenance m = new CGeneralMaintenance(0, 500, "test", new TimeStamp(9 ,15, 1991, new Time(1, 0, 0, 0)).toString(), new TimeStamp(8, 15,1991, new Time(1, 0, 0, 0)).toString());
        IFacility f = new CGeneralFacility(0, "", "", "", 0, 0);
        IMaintenanceRequest mq = new CMaintenanceRequest(55, f, m, new TimeStamp(9 ,15, 1991, new Time(1, 0, 0, 0)).toString(), false);
        maintSched.scheduleMaintenance(mq);
        assertNotNull(maintDAO.getMaintenanceRequest(55));
        assertTrue(maintSched.getMaintenanceRequestQueue().size() > 0);
        maintSched.doMaintenance(mq);
        assertTrue(maintSched.getMaintenanceRequestHistory().size() > 0);
        mq = maintDAO.getMaintenanceRequest(55);
        assertTrue(mq.getIsDone() == true);
        maintDAO.deleteMaintenanceRequest(mq);
    }

    @Test
    public void testGetMaintenanceRequestQueue() throws Exception {
        assertNotNull(maintSched.getMaintenanceRequestQueue());
    }

    @Test
    public void testSetMaintenanceRequestQueue() throws Exception {
        assertNotNull(maintSched.getMaintenanceRequestQueue());
        maintSched.setMaintenanceRequestQueue(new ArrayList<IMaintenanceRequest>());
        assertNotNull(maintSched.getMaintenanceRequestQueue());
    }

    @Test
    public void testGetMaintenanceRequestHistory() throws Exception {
        assertNotNull(maintSched.getMaintenanceRequestHistory());
    }

    @Test
    public void testSetMaintenanceRequestHistory() throws Exception {
        assertNotNull(maintSched.getMaintenanceRequestHistory());
        maintSched.setMaintenanceRequestHistory(new ArrayList<IMaintenanceRequest>());
        assertNotNull(maintSched.getMaintenanceRequestHistory());
    }
}
