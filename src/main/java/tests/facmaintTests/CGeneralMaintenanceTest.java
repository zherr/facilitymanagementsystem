package tests.facmaintTests;

import com.facility.system.model.facmaint.CGeneralMaintenance;
import com.facility.system.model.facmaint.IMaintenance;
import com.facility.system.model.utility.Time;
import com.facility.system.model.utility.TimeStamp;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 1/27/14.
 */
public class CGeneralMaintenanceTest {

    IMaintenance maintenance;

    @Before
    public void setUp() throws Exception {
        maintenance = new CGeneralMaintenance(0, 500, "fixing rafters", null, null);
    }

    @Test
    public void testCostMaint() throws Exception {
        assertTrue(maintenance.getCostOfMaintenance() == 500);
    }

    @Test
    public void testMaintDescr() throws Exception {
        assertTrue(maintenance.getMaintenanceDescription().equals("fixing rafters"));
    }

    @Test
    public void testMaintStart() throws Exception {
        assertTrue(maintenance.getMaintenanceStart() == null);
    }

    @Test
    public void testMaintEnd() throws Exception {
        assertTrue(maintenance.getMaintenanceEnd() == null);
    }

    @Test
    public void testSetCostOfMaintenance() throws Exception {
        maintenance.setCostOfMaintenance(600);
        assertTrue(maintenance.getCostOfMaintenance() == 600);
    }

    @Test
    public void testSetMaintenanceDescription() throws Exception {
        maintenance.setMaintenanceDescription("test");
        assertTrue(maintenance.getMaintenanceDescription().equals("test"));
    }

    @Test
    public void testSetMaintenanceStart() throws Exception {
        maintenance.setMaintenanceStart(new TimeStamp(0, 0, 0, new Time(0, 0, 0, 0)).toString());
        assertNotNull(maintenance.getMaintenanceStart());
    }

    @Test
    public void testSetMaintenanceEnd() throws Exception {
        maintenance.setMaintenanceEnd(new TimeStamp(0, 0, 0, new Time(0, 0, 0, 0)).toString());
        assertNotNull(maintenance.getMaintenanceEnd());
    }

    @Test
    public void testGetSetMaintId() throws Exception {
        assertTrue(maintenance.getMaintId() == 0);
        maintenance.setMaintId(1);
        assertTrue(maintenance.getMaintId() == 1);
    }
}
