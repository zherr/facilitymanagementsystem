package tests.facoperationTests;

import com.facility.system.dal.*;
import com.facility.system.model.facoperation.*;
import com.facility.system.model.facoverview.CGeneralFacility;
import com.facility.system.model.facoverview.COwnedFacility;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.user.CRenter;
import com.facility.system.model.utility.Time;
import com.facility.system.model.utility.TimeStamp;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * Created by zherr on 1/27/14.
 */
public class CFacilityOperatorTest {

    IFacilityOperator facOp;

    IInspectionDAO inspectionDAO;
    IReservationDAO reservationDAO;
    IFacilityReserver facilityReserver;

    @Before
    public void setUp() throws Exception {
        inspectionDAO = new CInspectionDAO();
        reservationDAO = new CReservationDAO();
        facilityReserver = new CFacilityReserver(reservationDAO, new ArrayList<IReservation>());
        facOp = new CFacilityOperator();//new CFacilityReserver(new CReservationDAO(), new ArrayList<IReservation>()), new CInspectionDAO());
        facOp.setInspectionDAO(inspectionDAO);
        facOp.setFacilityReserver(facilityReserver);
    }

    @Test
    public void testIsInUseDuringInterval() throws Exception {
        COwnedFacilityDAO facDAO = new COwnedFacilityDAO();
        IFacility fac0 = facDAO.getFacility(0);
        assertNotNull(fac0);
        assertTrue(facOp.IsInUseDuringInterval(fac0, new TimeStamp(1, 31, 2014, new Time(23, 0, 0, 0)), new TimeStamp(2, 3, 2014, new Time(23, 30, 0, 0))));
        assertFalse(facOp.IsInUseDuringInterval(fac0, new TimeStamp(2, 4, 2014, new Time(22, 0, 0, 0)), new TimeStamp(2, 5, 2014, new Time(23, 30, 0, 0))));
    }

    @Test
    public void testAssignFacilityToUse() throws Exception {
        COwnedFacilityDAO facDAO = new COwnedFacilityDAO();
        IFacility fac0 = new COwnedFacility();
        fac0 = facDAO.getFacility(0);
        facOp.assignFacilityToUse(new CReservation(55, fac0, new TimeStamp(1, 31, 2014, new Time(23, 0, 0, 0)).toString(),
                new TimeStamp(2, 1, 2014, new Time(23, 30, 0, 0)).toString(), "test", 40, new CRenter(112233445, "renterTest")));
        CReservationDAO resDAO = new CReservationDAO();
        System.err.println(resDAO.getReservation(55).getTitle());
        assertTrue(resDAO.getReservation(55).getTitle().equals("test"));
        resDAO.deleteReservation(new CReservation(55, fac0, new TimeStamp(1, 31, 2014, new Time(23, 0, 0, 0)).toString(),
                new TimeStamp(2, 1, 2014, new Time(23, 30, 0, 0)).toString(), "test", 40, new CRenter(112233445, "renterTest")));
    }

    @Test
    public void testVacateFacility() throws Exception {
        IReservationDAO resDAO = new CReservationDAO();
        CFacilityDAO facDAO = new CFacilityDAO();
        IFacility fac0 = new CGeneralFacility();
        fac0 = facDAO.getFacility(0);
        List<IReservation> resList = new ArrayList<IReservation>();
        resList = resDAO.getAllReservations(fac0);
        facOp.vacateFacility(fac0);
        assertTrue(resDAO.getAllReservations(fac0).size() == 0);
        for(IReservation res : resList){
            resDAO.addReservation(res);
        }
    }

    @Test
    public void testListInspections() throws Exception {
        IReservationDAO resDAO = new CReservationDAO();
        CFacilityDAO facDAO = new CFacilityDAO();
        IFacility fac0 = facDAO.getFacility(0);
        assertTrue(facOp.listInspections(fac0).size() > 0);
    }

    @Test
    public void testListReservations() throws Exception {
        IReservationDAO resDAO = new CReservationDAO();
        CFacilityDAO facDAO = new CFacilityDAO();
        IFacility fac0 = facDAO.getFacility(0);
        assertTrue(facOp.listReservations(fac0).size() > 0);
    }

    @Test
    public void testCalcUsageRate() throws Exception {
        IReservationDAO resDAO = new CReservationDAO();
        CFacilityDAO facDAO = new CFacilityDAO();
        IFacility fac0 = facDAO.getFacility(0);
        assertTrue(facOp.calcUsageRate(fac0) == 1);
    }
}
