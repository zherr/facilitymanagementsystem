package tests.facoperationTests;

import com.facility.system.model.facoperation.CReservation;
import com.facility.system.model.user.CRenter;
import com.facility.system.model.utility.Time;
import com.facility.system.model.utility.TimeStamp;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 1/27/14.
 */
public class CReservationTest {

    CReservation reservation;

    @Before
    public void setUp() throws Exception {
        reservation = new CReservation(0, null, null, null, "meeting", 10, null);
    }

    @Test
    public void testGetReservationId() throws Exception {
        assertTrue(reservation.getReserveId() == 0);
    }

    @Test
    public void testGetFacilityToReserve() throws Exception {
        assertTrue(reservation.getFacToReserve() == null);
    }

    @Test
    public void testStart() throws Exception {
        assertTrue(reservation.getStart() == null);
    }

    @Test
    public void testEnd() throws Exception {
        assertTrue(reservation.getEnd() == null);
    }

    @Test
    public void testGetTitle() throws Exception {
        assertTrue(reservation.getTitle().equals("meeting"));
    }

    @Test
    public void testNumParticipants() throws Exception {
        reservation.setNumOfParticipants(1);
        assertTrue(reservation.getNumOfParticipants() == 1);
    }

    @Test
    public void testSetReservationId() throws Exception {
        reservation.setReserveId(1);
        assertTrue(reservation.getReserveId() == 1);
    }

    @Test
    public void testSetFacilityToReserve() throws Exception {
        reservation.setFacToReserve(null);
        assertTrue(reservation.getFacToReserve() == null);
    }

    @Test
    public void testSetStart() throws Exception {
        reservation.setStart(new TimeStamp(9, 9, 1991, new Time(0, 0, 0, 0)).toString());
        TimeStamp t = new TimeStamp(reservation.getStart());
        assertTrue(t.getDay() == 9);
    }

    @Test
    public void testSetEnd() throws Exception {
        reservation.setEnd(new TimeStamp(9, 9, 1991, new Time(0, 0, 0, 0)).toString());
        TimeStamp t = new TimeStamp(reservation.getEnd());
        assertTrue(t.getDay() == 9);
    }

    @Test
    public void testSetTitle() throws Exception {
        reservation.setTitle("wut");
        assertTrue(reservation.getTitle().equals("wut"));
    }

    @Test
    public void testSetNumParticipants() throws Exception {
        reservation.setNumOfParticipants(100);
        assertTrue(reservation.getNumOfParticipants() == 100);
    }

    @Test
    public void testSetGetRenter() {
        reservation.setRenter(new CRenter(0, "Renter1"));
        assertNotNull(reservation.getRenter());
        assertTrue(reservation.getRenter().getSsn() == 0);
        assertTrue(reservation.getRenter().getName().equals("Renter1"));
    }
}
