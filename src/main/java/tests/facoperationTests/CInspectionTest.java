package tests.facoperationTests;

import com.facility.system.model.facoperation.CInspection;
import com.facility.system.model.facoperation.IInspection;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by zherr on 1/27/14.
 */
public class CInspectionTest {

    IInspection inspection;

    @Before
    public void setUp() throws Exception {
        inspection = new CInspection(0, "bug inspection", null);
    }

    @Test
    public void testGetInspectId() throws Exception {
        assertTrue(inspection.getInspectId() == 0);
    }

    @Test
    public void testGetInspectionName() throws Exception {
        assertTrue(inspection.getInspectionName().equals("bug inspection"));
    }

    @Test
    public void testGetInspectFacility() throws Exception {
        assertTrue(inspection.getInspectFacility() == null);
    }

    @Test
    public void testSetInspectId() throws Exception {
        inspection.setInspectId(1);
        assertTrue(inspection.getInspectId() == 1);
    }

    @Test
    public void testSetInspectionName() throws Exception {
        inspection.setInspectionName("test");
        assertTrue(inspection.getInspectionName().equals("test"));
    }

    @Test
    public void testSetInspectFacility() throws Exception {
        inspection.setInspectFacility(null);
        assertTrue(inspection.getInspectFacility() == null);
    }
}
