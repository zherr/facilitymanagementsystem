package tests.facoperationTests;

import com.facility.system.dal.CFacilityDAO;
import com.facility.system.dal.CReservationDAO;
import com.facility.system.dal.IFacilityDAO;
import com.facility.system.dal.IReservationDAO;
import com.facility.system.model.facoperation.CFacilityReserver;
import com.facility.system.model.facoperation.CReservation;
import com.facility.system.model.facoperation.IFacilityReserver;
import com.facility.system.model.facoperation.IReservation;
import com.facility.system.model.facoverview.CGeneralFacility;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.user.CRenter;
import com.facility.system.model.utility.Time;
import com.facility.system.model.utility.TimeStamp;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNull;

/**
 * Created by zherr on 1/27/14.
 */
public class CFacilityReserverTest {

    IFacilityReserver facilityReserver;
    IFacility fac0;
    IFacilityDAO facDAO;
    IReservationDAO resDAO;

    @Before
    public void setUp() throws Exception {
        facilityReserver = new CFacilityReserver();
        facilityReserver.setReservations(new ArrayList<IReservation>());
        facilityReserver.setReservationDAO(new CReservationDAO());
        fac0 = new CGeneralFacility(0, "test", "", "", 0, 0);
        facDAO = new CFacilityDAO();
        resDAO = new CReservationDAO();
    }

    @Test
    public void testUsageRate() throws Exception {
        assertTrue(facilityReserver.usageRate(fac0) > 0);
    }

    @Test
    public void testInUse() throws Exception {
        fac0 = facDAO.getFacility(0);
        assertNotNull(fac0);
        assertTrue(facilityReserver.inUse(fac0, new TimeStamp(2, 1, 2014, new Time(22, 0, 0, 0)), new TimeStamp(2, 8, 2014, new Time(23, 30, 0, 0))));
        assertFalse(facilityReserver.inUse(fac0, new TimeStamp(1, 20, 2014, new Time(22, 0, 0, 0)), new TimeStamp(1, 25, 2014, new Time(23, 30, 0, 0))));
    }

    @Test
    public void testAddReservation() throws Exception {
        IReservation r = new CReservation(55, new CGeneralFacility(0, "", "", "", 0, 0), new TimeStamp(9, 15, 1991,
                new Time(0, 0, 0 ,0)).toString(), new TimeStamp(9, 15, 1991, new Time(0, 0, 0 ,0)).toString(), "test", 5, new CRenter(1234567890, "renterTest"));
        facilityReserver.addReservation(r);
        assertNotNull(resDAO.getReservation(55));
        resDAO.deleteReservation(r);
    }

    @Test
    public void testRemoveReservation() throws Exception {
        IReservation r = new CReservation(55, new CGeneralFacility(0, "", "", "", 0, 0), new TimeStamp(9, 15, 1991,
                new Time(0, 0, 0 ,0)).toString(), new TimeStamp(9, 15, 1991, new Time(0, 0, 0 ,0)).toString(), "test", 5, new CRenter(1234567890, "renterTest"));
        facilityReserver.addReservation(r);
        assertNotNull(resDAO.getReservation(55));
        facilityReserver.removeReservation(r);
        assertNull(resDAO.getReservation(55).getEnd());
    }

    @Test
    public void testRemoveAllReservations() throws Exception {
        // Empty
    }

    @Test
    public void testListAllReservations() throws Exception {
        // Empty
    }
}
