package com.facility.system.dal.hibernate;

import com.facility.system.dal.IReservationDAO;
import com.facility.system.model.facoperation.CReservation;
import com.facility.system.model.facoperation.IReservation;
import com.facility.system.model.facoverview.IFacility;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by zherr on 2/27/14.
 */
public class HibernateReservationDAO implements IReservationDAO {
    @Override
    public IReservation getReservation(int reserveId) {
        try {
            System.out.println("*************** Searching for reservation information in DB with ID ...  " + reserveId);
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getCustQuery = session.createQuery("From CReservation where reserveId=:reserveId");
            getCustQuery.setInteger("reserveId", reserveId);

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

            List reservations = getCustQuery.list();
            System.out.println("Getting reservation details using HQL. \n" + reservations);

            session.getTransaction().commit();
            if(reservations.size() > 0) {
                return (CReservation)reservations.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addReservation(IReservation reservation) {
        System.out.println("*************** Adding reservation in DB with ID ...  " + reservation.getReserveId());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(reservation);
        session.getTransaction().commit();
    }

    @Override
    public List<IReservation> getAllReservations(IFacility facility) {
        try {
            System.out.println("*************** Searching for all reservations for facility in DB");
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getCustQuery = session.createQuery("From CReservation where facToReserve=:facToReserve");
            getCustQuery.setInteger("facToReserve", facility.getFacilityId());

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

            List reservations = getCustQuery.list();
            System.out.println("Getting reservations details using HQL. \n" + reservations);

            session.getTransaction().commit();
            if(reservations.size() > 0) {
                return reservations;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteReservation(IReservation reservation) {
        System.out.println("*************** Deleting reservation in DB with ID ...  " + reservation.getReserveId());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.delete(reservation);
        session.getTransaction().commit();
    }

    @Override
    public void updateReservation(IReservation reservation) {
        // Empt
    }

    @Override
    public void deleteAllReservations(IFacility facility) {
        try {
            System.out.println("*************** Searching for all reservations for facility in DB");
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getCustQuery = session.createQuery("Delete CReservation where facToReserve=:facToReserve");
            getCustQuery.setInteger("facToReserve", facility.getFacilityId());
            getCustQuery.executeUpdate();

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

//            List reservations = getCustQuery.list();
//            if(reservations.size() > 0) {
//                for(Object r : reservations) {
//                    COwnedFacility oR = (COwnedFacility) r;
//                    session.delete(oR);
//                }
//            }
//            System.out.println("Getting reservations details using HQL. \n" + reservations);

            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
