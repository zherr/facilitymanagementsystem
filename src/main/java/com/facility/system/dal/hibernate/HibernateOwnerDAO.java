package com.facility.system.dal.hibernate;

import com.facility.system.dal.IOwnerDAO;
import com.facility.system.model.user.COwner;
import com.facility.system.model.user.IUser;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by zherr on 2/27/14.
 */
public class HibernateOwnerDAO implements IOwnerDAO {
    @Override
    public void addOwner(IUser user) {
        System.out.println("*************** Adding owner information in DB with ID ...  " + user.getSsn());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
    }

    @Override
    public void deleteOwner(IUser user) {
        System.out.println("*************** Deleting owner information in DB with ID ...  " + user.getSsn());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.delete(user);
        session.getTransaction().commit();
    }

    @Override
    public void updateOwner(IUser user) {
        // Empty TODO
    }

    @Override
    public IUser getOwner(int ssn) {
        try {
            System.out.println("*************** Searching for owner information in DB with ID ...  " + ssn);
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getCustQuery = session.createQuery("From COwner where ssn=:ssn");
            getCustQuery.setInteger("ssn", ssn);

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

            List owners = getCustQuery.list();
            System.out.println("Getting owner details using HQL. \n" + owners);

            session.getTransaction().commit();
            if(owners.size() > 0) {
                return (COwner)owners.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<IUser> getAllOwners() {
        // Empty TODO
        return null;
    }
}
