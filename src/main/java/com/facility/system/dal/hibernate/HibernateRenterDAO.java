package com.facility.system.dal.hibernate;

import com.facility.system.dal.IRenterDAO;
import com.facility.system.model.user.CRenter;
import com.facility.system.model.user.IUser;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by zherr on 2/27/14.
 */
public class HibernateRenterDAO implements IRenterDAO {
    @Override
    public void addRenter(IUser user) {
        System.out.println("*************** Adding renter information in DB with ID ...  " + user.getSsn());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
    }

    @Override
    public void deleteRenter(IUser user) {
        System.out.println("*************** Deleting renter information in DB with ID ...  " + user.getSsn());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.delete(user);
        session.getTransaction().commit();
    }

    @Override
    public void updateRenter(IUser user) {

    }

    @Override
    public IUser getRenter(int ssn) {
        try {
            System.out.println("*************** Searching for renter information in DB with ID ...  " + ssn);
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getCustQuery = session.createQuery("From CRenter where ssn=:ssn");
            getCustQuery.setInteger("ssn", ssn);

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

            List renters = getCustQuery.list();
            System.out.println("Getting renter details using HQL. \n" + renters);

            session.getTransaction().commit();
            if(renters.size() > 0) {
                return (CRenter)renters.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<IUser> getAllRenters() {
        // Empty TODO
        return null;
    }
}
