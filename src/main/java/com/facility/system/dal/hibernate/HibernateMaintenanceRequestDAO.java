package com.facility.system.dal.hibernate;

import com.facility.system.dal.IMaintenanceRequestDAO;
import com.facility.system.model.facmaint.CMaintenanceRequest;
import com.facility.system.model.facmaint.IMaintenanceRequest;
import com.facility.system.model.facoverview.IFacility;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by zherr on 2/27/14.
 */
public class HibernateMaintenanceRequestDAO implements IMaintenanceRequestDAO {
    @Override
    public IMaintenanceRequest getMaintenanceRequest(int id) {
        try {
            System.out.println("*************** Searching for maint request information in DB with ID ...  " + id);
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getCustQuery = session.createQuery("From CMaintenanceRequest where maintReqId=:maintReqId");
            getCustQuery.setInteger("maintReqId", id);

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

            List maintReqs = getCustQuery.list();
            System.out.println("Getting maint req details using HQL. \n" + maintReqs);

            session.getTransaction().commit();
            if(maintReqs.size() > 0) {
                return (CMaintenanceRequest)maintReqs.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addMaintenanceRequest(IMaintenanceRequest maintenanceRequest) {
        System.out.println("*************** Adding Maintenance Request in DB with ID ...  " + maintenanceRequest.getMaintReqId());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(maintenanceRequest);
        session.getTransaction().commit();
    }

    @Override
    public List<IMaintenanceRequest> getAllMaintenanceRequest(IFacility facility) {
        try {
            System.out.println("*************** Searching for all maint reqs for facility in DB");
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getCustQuery = session.createQuery("From CMaintenanceRequest where facRequested=:facRequested");
            getCustQuery.setInteger("facRequested", facility.getFacilityId());

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

            List maintReqs = getCustQuery.list();
            System.out.println("Getting maint req details using HQL. \n" + maintReqs);

            session.getTransaction().commit();
            if(maintReqs.size() > 0) {
                return maintReqs;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteMaintenanceRequest(IMaintenanceRequest maintenanceRequest) {
        System.out.println("*************** Deleting Maintenance Request in DB with ID ...  " + maintenanceRequest.getMaintReqId());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.delete(maintenanceRequest);
        session.getTransaction().commit();
    }

    @Override
    public void updateMaintenanceRequest(IMaintenanceRequest maintenanceRequest) {
        System.out.println("*************** Updating Maintenance Request in DB with ID ...  " + maintenanceRequest.getMaintReqId());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.update(maintenanceRequest);
        session.getTransaction().commit();
    }

    @Override
    public void addMaintenanceRequestToHistory(IMaintenanceRequest maintenanceRequest) {
        maintenanceRequest.setIsDone(true);
        updateMaintenanceRequest(maintenanceRequest);
    }

    @Override
    public List<IMaintenanceRequest> getAllMaintenanceRequestsFromHistory(IFacility facility) {
        try {
            System.out.println("*************** Searching for all maint reqs for facility in DB");
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getCustQuery = session.createQuery("From CMaintenanceRequest where facRequested=:facRequested and " +
                    "isDone=:isDone");
            getCustQuery.setInteger("facRequested", facility.getFacilityId());
            getCustQuery.setBoolean("isDone", true);

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

            List maintReqs = getCustQuery.list();
            System.out.println("Getting maint req details using HQL. \n" + maintReqs);

            session.getTransaction().commit();
            if(maintReqs.size() > 0) {
                return maintReqs;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<IMaintenanceRequest> getAllMaintenanceRequestsCurrent(IFacility facility) {
        try {
            System.out.println("*************** Searching for all maint reqs for facility in DB");
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getCustQuery = session.createQuery("From CMaintenanceRequest where facRequested=:facRequested and " +
                    "isDone=:isDone");
            getCustQuery.setInteger("facRequested", facility.getFacilityId());
            getCustQuery.setBoolean("isDone", false);

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

            List maintReqs = getCustQuery.list();
            System.out.println("Getting maint req details using HQL. \n" + maintReqs);

            session.getTransaction().commit();
            if(maintReqs.size() > 0) {
                return maintReqs;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
