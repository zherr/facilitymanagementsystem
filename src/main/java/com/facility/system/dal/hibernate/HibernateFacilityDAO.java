package com.facility.system.dal.hibernate;

import com.facility.system.dal.IFacilityDAO;
import com.facility.system.model.facoverview.COwnedFacility;
import com.facility.system.model.facoverview.IFacility;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by zherr on 2/27/14.
 */
public class HibernateFacilityDAO implements IFacilityDAO {
    @Override
    public IFacility getFacility(int facId) {
        try {
            System.out.println("*************** Searching for facility information in DB with ID ...  " + facId);
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getCustQuery = session.createQuery("From COwnedFacility where FacNumber=:FacNumber");
            getCustQuery.setInteger("FacNumber", facId);

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

            List facilities = getCustQuery.list();
            System.out.println("Getting facility details using HQL. \n" + facilities);

            session.getTransaction().commit();
            if(facilities.size() > 0) {
                return (COwnedFacility)facilities.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addFacility(IFacility fac) {
        System.out.println("*************** Adding for facility in DB with ID ...  " + fac.getFacilityId());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(fac);
        session.getTransaction().commit();
    }

    @Override
    public List<IFacility> getAllFacilities() {
        try {
            System.out.println("*************** Searching for all facilities in DB");
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getCustQuery = session.createQuery("From COwnedFacility");

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

            List facilities = getCustQuery.list();
            System.out.println("Getting owner details using HQL. \n" + facilities);

            session.getTransaction().commit();
            if(facilities.size() > 0) {
                return facilities;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteFacility(IFacility fac) {
        System.out.println("*************** Deleting facility in DB with ID ...  " + fac.getFacilityId());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.delete(fac);
        session.getTransaction().commit();
    }

    @Override
    public void updateFacility(IFacility fac) {
        System.out.println("*************** Updating facility in DB with ID ...  " + fac.getFacilityId());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.update(fac);
        session.getTransaction().commit();
    }
}
