package com.facility.system.dal.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by zherr on 2/27/14.
 */
public class HibernateMySQLHelper {
    private static final SessionFactory sessionFactory;
//    private static final ServiceRegistry serviceRegistry;

    static {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
//            Configuration config = new Configuration();
//            config.configure();
//            serviceRegistry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
            sessionFactory = new Configuration().configure().buildSessionFactory();//config.buildSessionFactory(serviceRegistry);
            System.out.println("*************** Session Factory instantiated ..................");
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
