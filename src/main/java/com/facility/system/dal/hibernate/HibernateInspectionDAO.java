package com.facility.system.dal.hibernate;

import com.facility.system.dal.IInspectionDAO;
import com.facility.system.model.facoperation.CInspection;
import com.facility.system.model.facoperation.IInspection;
import com.facility.system.model.facoverview.IFacility;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by zherr on 2/27/14.
 */
public class HibernateInspectionDAO implements IInspectionDAO {
    @Override
    public IInspection getInspection(int id) {
        try {
            System.out.println("*************** Searching for inspection information in DB with ID ...  " + id);
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getCustQuery = session.createQuery("From CInspection where inspectId=:inspectId");
            getCustQuery.setInteger("inspectId", id);

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

            List inspections = getCustQuery.list();
            System.out.println("Getting inspection details using HQL. \n" + inspections);

            session.getTransaction().commit();
            if(inspections.size() > 0) {
                return (CInspection)inspections.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addInspection(IInspection inspection) {
        System.out.println("*************** Adding inspection in DB with ID ...  " + inspection.getInspectId());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(inspection);
        session.getTransaction().commit();
    }

    @Override
    public List<IInspection> getAllInspections(IFacility facility) {
        try {
            System.out.println("*************** Searching for inspection by facility information in DB with ID ...  " + facility.getFacilityId());
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getCustQuery = session.createQuery("From CInspection where inspectFacility=:inspectFacility");
            getCustQuery.setInteger("inspectFacility", facility.getFacilityId());

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

            List inspections = getCustQuery.list();
            System.out.println("Getting inspection details using HQL. \n" + inspections);

            session.getTransaction().commit();
            if(inspections.size() > 0) {
                return inspections;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteInspection(IInspection inspection) {
        System.out.println("*************** Deleting inspection in DB with ID ...  " + inspection.getInspectId());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.delete(inspection);
        session.getTransaction().commit();
    }

    @Override
    public void updateInspection(IInspection inspection) {
        // TODO empty
    }
}
