package com.facility.system.dal.hibernate;

import com.facility.system.dal.IMaintenanceDAO;
import com.facility.system.model.facmaint.CGeneralMaintenance;
import com.facility.system.model.facmaint.IMaintenance;
import com.facility.system.model.facoverview.IFacility;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by zherr on 2/27/14.
 */
public class HibernateMaintenanceDAO implements IMaintenanceDAO {
    @Override
    public IMaintenance getMaintenance(int id) {
        try {
            System.out.println("*************** Searching for maintenance information in DB with ID ...  " + id);
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Query getCustQuery = session.createQuery("From CGeneralMaintenance where maintId=:maintId");
            getCustQuery.setInteger("maintId", id);

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

            List maintenances = getCustQuery.list();
            System.out.println("Getting maintenance details using HQL. \n" + maintenances);

            session.getTransaction().commit();
            if(maintenances.size() > 0) {
                return (CGeneralMaintenance)maintenances.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addMaintenance(IMaintenance maintenance) {
        System.out.println("*************** Adding maintenance in DB with ID ...  " + maintenance.getMaintId());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(maintenance);
        session.getTransaction().commit();
    }

    @Override
    public List<IMaintenance> listAllMaintenance(IFacility facility) {
        try {
            System.out.println("*************** Searching for all maintenances for facility in DB");
            Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
            session.beginTransaction();
//            String selectMaintsQuery = "SELECT * FROM `maintenance` JOIN `maintenance_request` ON `maintenance_request`" +
//                    ".`maintNum` = `maintenance`.`maintId` WHERE `maintenance_request`.`facRequested` = " + facility.getFacilityId();
            Query getCustQuery = session.createQuery("Select m From CMaintenanceRequest mr join mr.maintenance m where mr.facilityRequested.facilityId=:facilityId");
            getCustQuery.setInteger("facilityId", facility.getFacilityId());

            System.out.println("*************** Retrieve Query is ....>>\n" + getCustQuery.toString());

            List maintenances = getCustQuery.list();
            System.out.println("Getting maintenance details using HQL. \n" + maintenances);

            session.getTransaction().commit();
            if(maintenances.size() > 0) {
                return maintenances;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteMaintenance(IMaintenance maintenance) {
        System.out.println("*************** Deleting maintenance in DB with ID ...  " + maintenance.getMaintId());
        Session session = HibernateMySQLHelper.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.delete(maintenance);
        session.getTransaction().commit();
    }

    @Override
    public void updateMaintenance(IMaintenance maintenance) {
        // Empty
    }
}
