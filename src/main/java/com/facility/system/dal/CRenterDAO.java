package com.facility.system.dal;

import com.facility.system.model.user.CRenter;
import com.facility.system.model.user.IUser;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zherr on 1/27/14.
 */
public class CRenterDAO implements IRenterDAO{

    public CRenterDAO() {}

    @Override
    public void addRenter(IUser user) {

        Connection con = DBHelper.getConnection();
        PreparedStatement renterPst = null;
        PreparedStatement addPst = null;

        try{
            String renterStmt = "INSERT INTO `renter`(ssn, name) VALUES(?, ?)";
            renterPst = con.prepareStatement(renterStmt);
            renterPst.setInt(1, user.getSsn());
            renterPst.setString(2, user.getName());
            renterPst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("RenterDAO: Threw SQLException while adding Renter to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    renterPst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("Renter: Threw a SQLException saving the Renter object.");
                System.err.println(ex.getMessage());
            }
        }
    }

    @Override
    public void deleteRenter(IUser user) {

        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();
            String deleteSql = "DELETE FROM `renter` WHERE `ssn`= " + user.getSsn() + ";";
            int result = st.executeUpdate(deleteSql);
            st.close();
            con.close();
        } catch( SQLException e ){
            System.err.println("RenterDAO: Threw exception when trying to delete a renter");
            e.printStackTrace();
        }
    }

    @Override
    public void updateRenter(IUser user) {

        Connection con = DBHelper.getConnection();
        PreparedStatement renterPst = null;
        PreparedStatement updatePst = null;

        try{
            String renterSql = "UPDATE `renter` SET `ssn` = ?, `name` = ? WHERE `ssn` = " + user.getSsn();
            renterPst = con.prepareStatement(renterSql);
            renterPst.setInt(1, user.getSsn());
            renterPst.setString(2, user.getName());
            renterPst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("RenterDao: Threw SQLException while updating Renter to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (updatePst != null) {
                    updatePst.close();
                    renterPst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("RenterDao: Threw a SQLException updating the Renter object.");
                System.err.println(ex.getMessage());
            }
        }
    }

    @Override
    public IUser getRenter(int ssn) {
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();

            String selectRenterQuery = "SELECT * FROM `renter` WHERE `ssn` = '" + ssn + "'";

            ResultSet renterRS = st.executeQuery(selectRenterQuery);
            System.out.println("RenterDAO: *************** Query " + selectRenterQuery);

            CRenter renter = new CRenter();
            while ( renterRS.next() ) {
                renter.setSsn(ssn);
                renter.setName(renterRS.getString("name"));
            }
            //close to manage resources
            renterRS.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return renter;
        }
        catch( SQLException se ){
            System.err.println("RenterDAO: Threw a SQLException retrieving the renter object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    @Override
    public List<IUser> getAllRenters() {
        ArrayList<IUser> renters = new ArrayList<IUser>();
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();

            String selectRentersQuery = "SELECT * FROM `renter`";

            ResultSet renterRS = st.executeQuery(selectRentersQuery);
            System.out.println("RenterDAO: *************** Query " + selectRentersQuery);

            while ( renterRS.next() ) {
                CRenter renter = new CRenter();
                renter.setSsn(renterRS.getInt("ssn"));
                renter.setName(renterRS.getString("name"));
                renters.add(renter);
            }
            //close to manage resources
            renterRS.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return renters;
        }
        catch( SQLException se ){
            System.err.println("RenterDAO: Threw a SQLException retrieving the Renter object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

}
