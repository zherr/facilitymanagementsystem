package com.facility.system.dal;

import com.facility.system.model.facmaint.IMaintenance;
import com.facility.system.model.facoverview.IFacility;

import java.util.List;

/**
 * Created by zherr on 1/26/14.
 * DAO for maintenance
 */
public interface IMaintenanceDAO {
    public IMaintenance getMaintenance(int id);
    public void addMaintenance(IMaintenance maintenance);
    public List<IMaintenance> listAllMaintenance(IFacility facility);
    public void deleteMaintenance(IMaintenance maintenance);
    public void updateMaintenance(IMaintenance maintenance);
}
