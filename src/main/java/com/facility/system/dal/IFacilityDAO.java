package com.facility.system.dal;

import com.facility.system.model.facoverview.IFacility;

import java.util.List;

/**
 * Created by zherr on 1/26/14.
 * DAO for facilities
 */
public interface IFacilityDAO {
    public IFacility getFacility(int facId);
    public void addFacility(IFacility fac);
    public List<IFacility> getAllFacilities();
    public void deleteFacility(IFacility fac);
    public void updateFacility(IFacility fac);
}
