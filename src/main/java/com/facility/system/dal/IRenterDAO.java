package com.facility.system.dal;

import com.facility.system.model.user.IUser;

import java.util.List;

/**
 * Created by zherr on 1/26/14.
 * DAO for facilities
 */
public interface IRenterDAO {

    public void addRenter(IUser user);
    public void deleteRenter(IUser user);
    public void updateRenter(IUser user);
    public IUser getRenter(int ssn);
    public List<IUser> getAllRenters();

}
