package com.facility.system.dal;

import com.facility.system.model.facmaint.CMaintenanceRequest;
import com.facility.system.model.facmaint.IMaintenance;
import com.facility.system.model.facmaint.IMaintenanceRequest;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.utility.TimeStamp;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zherr on 1/27/14.
 */
public class CMaintenanceRequestDAO implements IMaintenanceRequestDAO{
    @Override
    public IMaintenanceRequest getMaintenanceRequest(int id) {
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();

            String selectMaintReqQuery = "SELECT * FROM `maintenance_request` WHERE `maintReqId` = '" + id + "'";

            ResultSet maintReqRS = st.executeQuery(selectMaintReqQuery);
            System.out.println("MaintReqDAO: *************** Query " + selectMaintReqQuery);

            CMaintenanceRequest maintReq = new CMaintenanceRequest();
            while ( maintReqRS.next() ) {
                maintReq.setMaintReqId(id);
                maintReq.setIsDone(maintReqRS.getBoolean("getIsDone"));
                int facId = maintReqRS.getInt("facRequested");
                int maintId = maintReqRS.getInt("maintNum");
                CFacilityDAO facDAO = new CFacilityDAO();
                CMaintenanceDAO maintDAO = new CMaintenanceDAO();
                IFacility fac = facDAO.getFacility(facId);
                assert(null != fac);
                IMaintenance maint = maintDAO.getMaintenance(maintId);
                assert(null != maint);
                maintReq.setFacilityRequested(fac);
                maintReq.setMaintenance(maint);
                TimeStamp time = new TimeStamp(maintReqRS.getString("timeRequested"));
                assert(null != time);
                maintReq.setTimeRequested(time.toString());
            }
            //close to manage resources
            maintReqRS.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return maintReq;
        }
        catch( SQLException se ){
            System.err.println("MaintReqDAO: Threw a SQLException retrieving the MaintenanceRequest object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    @Override
    public void addMaintenanceRequest(IMaintenanceRequest maintenanceRequest) {
        Connection con = DBHelper.getConnection();
        PreparedStatement maintenancePst = null;
        PreparedStatement addPst = null;

        try{
            String maintReqStmt = "INSERT INTO `maintenance_request`(maintReqId, facRequested, maintNum," +
                    " getIsDone, timeRequested) VALUES(?, ?, ?, ?, ?)";
            maintenancePst = con.prepareStatement(maintReqStmt);
            maintenancePst.setInt(1, maintenanceRequest.getMaintReqId());
            maintenancePst.setInt(2, maintenanceRequest.getFacilityRequested().getFacilityId());
            maintenancePst.setInt(3, maintenanceRequest.getMaintenance().getMaintId());
            maintenancePst.setBoolean(4, maintenanceRequest.getIsDone());
            maintenancePst.setString(5, maintenanceRequest.getTimeRequested().toString());
            maintenancePst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("MaintReqDAO: Threw SQLException while adding MaintenanceRequest to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    maintenancePst.close();
                }
                if (con != null) {
                    con.close();
                }
                con.close();
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("MaintenanceReqDAO: Threw a SQLException saving the MaintenanceReq object.");
                System.err.println(ex.getMessage());
            }
        }

    }

    @Override
    public List<IMaintenanceRequest> getAllMaintenanceRequest(IFacility facility) {
        ArrayList<IMaintenanceRequest> maintReqs = new ArrayList<IMaintenanceRequest>();
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();

            String selectMaintsQuery = "SELECT * FROM `maintenance_request` WHERE `maintenance_request`.`facRequested` = " + facility.getFacilityId();

            ResultSet maintReqRs = st.executeQuery(selectMaintsQuery);
            System.out.println("MaintenanceReqDAO: *************** Query " + selectMaintsQuery);

            while ( maintReqRs.next() ) {
                CMaintenanceRequest maintReq = new CMaintenanceRequest();
                maintReq.setMaintReqId(maintReqRs.getInt("maintReqId"));
                maintReq.setIsDone(maintReqRs.getBoolean("getIsDone"));
                int facId = maintReqRs.getInt("facRequested");
                int maintId = maintReqRs.getInt("maintNum");
                CFacilityDAO facDAO = new CFacilityDAO();
                CMaintenanceDAO maintDAO = new CMaintenanceDAO();
                IFacility fac = facDAO.getFacility(facId);
                assert(null != fac);
                IMaintenance maint = maintDAO.getMaintenance(maintId);
                assert(null != maint);
                maintReq.setFacilityRequested(fac);
                maintReq.setMaintenance(maint);
                TimeStamp time = new TimeStamp(maintReqRs.getString("timeRequested"));
                System.err.println("\n\n\n" + time.toString() + "\n\n\n");
                assert(null != time);
                maintReq.setTimeRequested(time.toString());
                maintReqs.add(maintReq);
            }
            //close to manage resources
            maintReqRs.close();
            st.close();
            DBHelper.closeConnection();

            return maintReqs;
        }
        catch( SQLException se ){
            System.err.println("MaintenanceReqDAO: Threw a SQLException retrieving the MaintenanceReq object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteMaintenanceRequest(IMaintenanceRequest maintenanceRequest) {
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();
            String deleteSql = "DELETE FROM `maintenance_request` WHERE `maintReqId`= " + maintenanceRequest.getMaintReqId() + ";";
            int result = st.executeUpdate(deleteSql);
            st.close();
            con.close();
        } catch( SQLException e ){
            System.err.println("MaintenanceReqDAO: Threw exception when trying to delete a MaintenanceRequest");
            e.printStackTrace();
        }
    }

    @Override
    public void updateMaintenanceRequest(IMaintenanceRequest maintenanceRequest) {
        Connection con = DBHelper.getConnection();
        PreparedStatement maintenancePst = null;
        PreparedStatement updatePst = null;

        try{
            String maintReqStmt = "UPDATE `maintenance_request` SET `getIsDone` = ?, `timeRequested` = ? WHERE `maintReqId` = " + maintenanceRequest.getMaintReqId();
            maintenancePst = con.prepareStatement(maintReqStmt);
            maintenancePst.setBoolean(1, maintenanceRequest.getIsDone());
            maintenancePst.setString(2, maintenanceRequest.getTimeRequested().toString());
            maintenancePst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("MaintReqDAO: Threw SQLException while updating MaintenanceRequest to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (updatePst != null) {
                    updatePst.close();
                    maintenancePst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("MaintenanceReqDAO: Threw a SQLException updating the MaintenanceReq object.");
                System.err.println(ex.getMessage());
            }
        }
    }

    /**
     * This simply flips a maintenance request from false to true
     * @param maintenanceRequest
     */
    @Override
    public void addMaintenanceRequestToHistory(IMaintenanceRequest maintenanceRequest) {
        IMaintenanceRequest m = maintenanceRequest;
        m.setIsDone(true);
        updateMaintenanceRequest(m);
    }

    @Override
    public List<IMaintenanceRequest> getAllMaintenanceRequestsFromHistory(IFacility facility) {
        ArrayList<IMaintenanceRequest> maintReqs = new ArrayList<IMaintenanceRequest>();
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();

            String selectMaintsQuery = "SELECT * FROM `maintenance_request` WHERE `maintenance_request`.`facRequested` = " + facility.getFacilityId()
                    + " AND `maintenance_request`.`getIsDone` = true";

            ResultSet maintReqRs = st.executeQuery(selectMaintsQuery);
            System.out.println("MaintenanceReqDAO: *************** Query " + selectMaintsQuery);

            while ( maintReqRs.next() ) {
                CMaintenanceRequest maintReq = new CMaintenanceRequest();
                maintReq.setMaintReqId(maintReqRs.getInt("maintReqId"));
                maintReq.setIsDone(maintReqRs.getBoolean("getIsDone"));
                int facId = maintReqRs.getInt("facRequested");
                int maintId = maintReqRs.getInt("maintNum");
                CFacilityDAO facDAO = new CFacilityDAO();
                CMaintenanceDAO maintDAO = new CMaintenanceDAO();
                IFacility fac = facDAO.getFacility(facId);
                assert(null != fac);
                IMaintenance maint = maintDAO.getMaintenance(maintId);
                assert(null != maint);
                maintReq.setFacilityRequested(fac);
                maintReq.setMaintenance(maint);
                TimeStamp time = new TimeStamp(maintReqRs.getString("timeRequested"));
                System.err.println("\n\n\n" + time.toString() + "\n\n\n");
                assert(null != time);
                maintReq.setTimeRequested(time.toString());
                maintReqs.add(maintReq);
            }
            //close to manage resources
            maintReqRs.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return maintReqs;
        }
        catch( SQLException se ){
            System.err.println("MaintenanceReqDAO: Threw a SQLException retrieving the MaintenanceReq object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    @Override
    public List<IMaintenanceRequest> getAllMaintenanceRequestsCurrent(IFacility facility) {
        ArrayList<IMaintenanceRequest> maintReqs = new ArrayList<IMaintenanceRequest>();
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();

            String selectMaintsQuery = "SELECT * FROM `maintenance_request` WHERE `maintenance_request`.`facRequested` = " + facility.getFacilityId()
                    + " AND `maintenance_request`.`getIsDone` = false";

            ResultSet maintReqRs = st.executeQuery(selectMaintsQuery);
            System.out.println("MaintenanceReqDAO: *************** Query " + selectMaintsQuery);

            while ( maintReqRs.next() ) {
                CMaintenanceRequest maintReq = new CMaintenanceRequest();
                maintReq.setMaintReqId(maintReqRs.getInt("maintReqId"));
                maintReq.setIsDone(maintReqRs.getBoolean("getIsDone"));
                int facId = maintReqRs.getInt("facRequested");
                int maintId = maintReqRs.getInt("maintNum");
                CFacilityDAO facDAO = new CFacilityDAO();
                CMaintenanceDAO maintDAO = new CMaintenanceDAO();
                IFacility fac = facDAO.getFacility(facId);
                assert(null != fac);
                IMaintenance maint = maintDAO.getMaintenance(maintId);
                assert(null != maint);
                maintReq.setFacilityRequested(fac);
                maintReq.setMaintenance(maint);
                TimeStamp time = new TimeStamp(maintReqRs.getString("timeRequested"));
                System.err.println("\n\n\n" + time.toString() + "\n\n\n");
                assert(null != time);
                maintReq.setTimeRequested(time.toString());
                maintReqs.add(maintReq);
            }
            //close to manage resources
            maintReqRs.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return maintReqs;
        }
        catch( SQLException se ){
            System.err.println("MaintenanceReqDAO: Threw a SQLException retrieving the MaintenanceReq object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }
}
