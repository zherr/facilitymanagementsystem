package com.facility.system.dal;

import com.facility.system.model.facoperation.CInspection;
import com.facility.system.model.facoperation.IInspection;
import com.facility.system.model.facoverview.IFacility;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zherr on 1/27/14.
 */
public class CInspectionDAO implements IInspectionDAO{

    public CInspectionDAO() {}

    @Override
    public IInspection getInspection(int id) {
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();

            String selectInspectionQuery = "SELECT * FROM `inspection` WHERE `inspectId` = '" + id + "'";

            ResultSet inspectionRS = st.executeQuery(selectInspectionQuery);
            System.out.println("InspectionDAO: *************** Query " + selectInspectionQuery);

            CInspection inspection = new CInspection();
            while ( inspectionRS.next() ) {
                inspection.setInspectId(id);
                inspection.setInspectionName(inspectionRS.getString("inspectionName"));
                CFacilityDAO facDao = new CFacilityDAO();
                int inspectFac = inspectionRS.getInt("inspectFacility");
                IFacility inspectFacility = facDao.getFacility(inspectFac);
                inspection.setInspectFacility(inspectFacility);
            }
            //close to manage resources
            inspectionRS.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return inspection;
        }
        catch( SQLException se ){
            System.err.println("InspectionDAO: Threw a SQLException retrieving the Inspection object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    @Override
    public void addInspection(IInspection inspection) {
        Connection con = DBHelper.getConnection();
        PreparedStatement inspectionPst = null;
        PreparedStatement addPst = null;

        try{
            String facilityStmt = "INSERT INTO `inspection`(InspectId, inspectFacility, inspectionName) VALUES(?, ?, ?)";
            inspectionPst = con.prepareStatement(facilityStmt);
            inspectionPst.setInt(1, inspection.getInspectId());
            inspectionPst.setInt(2, inspection.getInspectFacility().getFacilityId());
            inspectionPst.setString(3, inspection.getInspectionName());
            inspectionPst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("InspectionDAO: Threw SQLException while adding Inspection to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    inspectionPst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("Facility: Threw a SQLException saving the Inspection object.");
                System.err.println(ex.getMessage());
            }
        }
    }

    @Override
    public List<IInspection> getAllInspections(IFacility facility) {
        ArrayList<IInspection> inspections = new ArrayList<IInspection>();
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();

            String selectInspectionsQuery = "SELECT * FROM `inspection`";

            ResultSet inspectionsRS = st.executeQuery(selectInspectionsQuery);
            System.out.println("InspectionDAO: *************** Query " + selectInspectionsQuery);

            while ( inspectionsRS.next() ) {
                CInspection inspection = new CInspection();
                inspection.setInspectId(inspectionsRS.getInt("inspectId"));
                inspection.setInspectionName(inspectionsRS.getString("inspectionName"));
                CFacilityDAO facDao = new CFacilityDAO();
                int inspectFac = inspectionsRS.getInt("inspectFacility");
                IFacility inspectFacility = facDao.getFacility(inspectFac);
                inspection.setInspectFacility(inspectFacility);
                inspections.add(inspection);
            }
            //close to manage resources
            inspectionsRS.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return inspections;
        }
        catch( SQLException se ){
            System.err.println("inspectionsDAO: Threw a SQLException retrieving the Inspection object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteInspection(IInspection inspection) {
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();
            String deleteSql = "DELETE FROM `inspection` WHERE `inspectId`= " + inspection.getInspectId() + ";";
            int result = st.executeUpdate(deleteSql);
            st.close();
            con.close();
        } catch( SQLException e ){
            System.err.println("InspectionDAO: Threw exception when trying to delete an Inspection");
            e.printStackTrace();
        }
    }

    @Override
    public void updateInspection(IInspection inspection) {
        //Empty
    }
}
