package com.facility.system.dal;

import com.facility.system.model.facmaint.CGeneralMaintenance;
import com.facility.system.model.facmaint.IMaintenance;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.utility.TimeStamp;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zherr on 1/27/14.
 */
public class CMaintenanceDAO implements IMaintenanceDAO{

    public CMaintenanceDAO() {}

    @Override
    public IMaintenance getMaintenance(int id) {
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();

            String selectMaintQuery = "SELECT * FROM `maintenance` WHERE `maintId` = '" + id + "'";

            ResultSet mainRS = st.executeQuery(selectMaintQuery);
            System.out.println("MaintenanceDAO: *************** Query " + selectMaintQuery);

            CGeneralMaintenance maintenance = new CGeneralMaintenance();
            while ( mainRS.next() ) {
                maintenance.setMaintId(id);
                maintenance.setCostOfMaintenance(mainRS.getInt("costOfMaintenance"));
                maintenance.setMaintenanceDescription(mainRS.getString("maintenanceDescription"));
                TimeStamp start = new TimeStamp(mainRS.getString("maintenanceStart"));
                assert(null != start);
                maintenance.setMaintenanceStart(start.toString());
                TimeStamp end = new TimeStamp(mainRS.getString("maintenanceEnd"));
                assert(null != end);
                maintenance.setMaintenanceEnd(end.toString());
            }
            //close to manage resources
            mainRS.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return maintenance;
        }
        catch( SQLException se ){
            System.err.println("MaintenanceDAO: Threw a SQLException retrieving the Maintenance object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    @Override
    public void addMaintenance(IMaintenance maintenance) {
        Connection con = DBHelper.getConnection();
        PreparedStatement maintenancePst = null;
        PreparedStatement addPst = null;

        try{
            String maintStmt = "INSERT INTO `maintenance`(maintId, costOfMaintenance, maintenanceDescription," +
                    " maintenanceStart, maintenanceEnd) VALUES(?, ?, ?, ?, ?)";
            maintenancePst = con.prepareStatement(maintStmt);
            maintenancePst.setInt(1, maintenance.getMaintId());
            maintenancePst.setInt(2, maintenance.getCostOfMaintenance());
            maintenancePst.setString(3, maintenance.getMaintenanceDescription());
            maintenancePst.setString(4, maintenance.getMaintenanceStart().toString());
            maintenancePst.setString(5, maintenance.getMaintenanceEnd().toString());
            maintenancePst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("MaintenanceDAO: Threw SQLException while adding Maintenance to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    maintenancePst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("MaintenanceDAO: Threw a SQLException saving the Maintenance object.");
                System.err.println(ex.getMessage());
            }
        }
    }

    @Override
    public List<IMaintenance> listAllMaintenance(IFacility facility) {
        ArrayList<IMaintenance> maints = new ArrayList<IMaintenance>();
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();

            String selectMaintsQuery = "SELECT * FROM `maintenance` JOIN `maintenance_request` ON `maintenance_request`" +
                    ".`maintNum` = `maintenance`.`maintId` WHERE `maintenance_request`.`facRequested` = " + facility.getFacilityId();

            ResultSet maintsRS = st.executeQuery(selectMaintsQuery);
            System.out.println("MaintenanceDAO: *************** Query " + selectMaintsQuery);

            while ( maintsRS.next() ) {
                CGeneralMaintenance maintenance = new CGeneralMaintenance();
                maintenance.setMaintId(maintsRS.getInt("maintId"));
                maintenance.setCostOfMaintenance(maintsRS.getInt("costOfMaintenance"));
                maintenance.setMaintenanceDescription(maintsRS.getString("maintenanceDescription"));
                TimeStamp start = new TimeStamp(maintsRS.getString("maintenanceStart"));
                assert(null != start);
                maintenance.setMaintenanceStart(start.toString());
                TimeStamp end = new TimeStamp(maintsRS.getString("maintenanceEnd"));
                assert(null != end);
                maintenance.setMaintenanceEnd(end.toString());
                maints.add(maintenance);
            }
            //close to manage resources
            maintsRS.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return maints;
        }
        catch( SQLException se ){
            System.err.println("MaintenanceDAO: Threw a SQLException retrieving the Maintenance object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteMaintenance(IMaintenance maintenance) {
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();
            String deleteSql = "DELETE FROM `maintenance` WHERE `maintId`= " + maintenance.getMaintId() + ";";
            int result = st.executeUpdate(deleteSql);
            st.close();
            con.close();
        } catch( SQLException e ){
            System.err.println("MaintenanceDAO: Threw exception when trying to delete a Maintenance");
            e.printStackTrace();
        }
    }

    @Override
    public void updateMaintenance(IMaintenance maintenance) {
        ///Empty
    }
}
