package com.facility.system.dal;

import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 10/4/13
 * Time: 10:50 AM
 * To change this template use File | Settings | File Templates.
 */
public class DBHelper {

    private static Connection connection;

    public static Connection getConnection() {

        System.out.println("DBHelper: -------- MySQL " + "JDBC Connection  ------------");

        try {

            Class.forName("com.mysql.jdbc.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("DBHelper: Check Where  your MySQL JDBC Driver exist and " + "Include in your library path!");
            e.printStackTrace();
            return null;

        }

        System.out.println("DBHelper: MySQL JDBC Driver Registered!");

        connection = null;

        try {

            // Heroku connection
            connection = DriverManager.getConnection("jdbc:mysql://us-cdbr-east-04.cleardb.com/heroku_584db720c56a7e4?", "badefc80053c9d", "46768d99");

            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT VERSION()");

            if (rs.next()) {
                System.out.println("DBHelper: The Database Version is " + rs.getString(1));
            }

        } catch (SQLException e) {

            System.out.println("DBHelper: Connection Failed! Check output console");
            e.printStackTrace();
            return null;

        }

        if (connection != null) {
            System.out.println("DBHelper: You have a database connection!");
        } else {
            System.out.println("DBHelper: Failed to make connection!");
        }

        return connection;
    }

    public static void closeConnection(){
        try{
            connection.close();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
}
