package com.facility.system.dal;

import com.facility.system.model.facoperation.IReservation;
import com.facility.system.model.facoverview.IFacility;

import java.util.List;

/**
 * Created by zherr on 1/26/14.
 * DAO for reservations
 */
public interface IReservationDAO {
    public IReservation getReservation(int reserveId);
    public void addReservation(IReservation reservation);
    public List<IReservation> getAllReservations(IFacility facility);
    public void deleteReservation(IReservation reservation);
    public void updateReservation(IReservation reservation);
    public void deleteAllReservations(IFacility facility);
}
