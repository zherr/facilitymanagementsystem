package com.facility.system.dal;

import com.facility.system.model.facoperation.IInspection;
import com.facility.system.model.facoverview.IFacility;

import java.util.List;

/**
 * Created by zherr on 1/26/14.
 * DAO for inspections
 */
public interface IInspectionDAO {
    public IInspection getInspection(int id);
    public void addInspection(IInspection inspection);
    public List getAllInspections(IFacility facility);
    public void deleteInspection(IInspection inspection);
    public void updateInspection(IInspection inspection);
}
