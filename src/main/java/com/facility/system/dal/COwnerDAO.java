package com.facility.system.dal;

import com.facility.system.model.user.COwner;
import com.facility.system.model.user.IUser;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zherr on 1/27/14.
 */
public class COwnerDAO implements IOwnerDAO{

    public COwnerDAO() {}

    @Override
    public void addOwner(IUser user) {

        Connection con = DBHelper.getConnection();
        PreparedStatement ownerPst = null;
        PreparedStatement addPst = null;

        try{
            String ownerStmt = "INSERT INTO `owner`(ssn, name) VALUES(?, ?)";
            ownerPst = con.prepareStatement(ownerStmt);
            ownerPst.setInt(1, user.getSsn());
            ownerPst.setString(2, user.getName());
            ownerPst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("OwnerDAO: Threw SQLException while adding Owner to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    ownerPst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("Owner: Threw a SQLException saving the Owner object.");
                System.err.println(ex.getMessage());
            }
        }
    }

    @Override
    public void deleteOwner(IUser user) {

        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();
            String deleteSql = "DELETE FROM `owner` WHERE `ssn`= " + user.getSsn() + ";";
            int result = st.executeUpdate(deleteSql);
            st.close();
            con.close();
        } catch( SQLException e ){
            System.err.println("OwnerDAO: Threw exception when trying to delete an owner");
            e.printStackTrace();
        }
    }

    @Override
    public void updateOwner(IUser user) {

        Connection con = DBHelper.getConnection();
        PreparedStatement ownerPst = null;
        PreparedStatement updatePst = null;

        try{
            String ownerSql = "UPDATE `owner` SET `ssn` = ?, `name` = ? WHERE `ssn` = " + user.getSsn();
            ownerPst = con.prepareStatement(ownerSql);
            ownerPst.setInt(1, user.getSsn());
            ownerPst.setString(2, user.getName());
            ownerPst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("OwnerDao: Threw SQLException while updating Owner to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (updatePst != null) {
                    updatePst.close();
                    ownerPst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("OwnerDao: Threw a SQLException updating the Owner object.");
                System.err.println(ex.getMessage());
            }
        }
    }

    @Override
    public IUser getOwner(int ssn) {
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();

            String selectOwnerQuery = "SELECT * FROM `owner` WHERE `ssn` = '" + ssn + "'";

            ResultSet ownerRS = st.executeQuery(selectOwnerQuery);
            System.out.println("OwnerDAO: *************** Query " + selectOwnerQuery);

            COwner owner = new COwner();
            while ( ownerRS.next() ) {
                owner.setSsn(ssn);
                owner.setName(ownerRS.getString("name"));
            }
            //close to manage resources
            ownerRS.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return owner;
        }
        catch( SQLException se ){
            System.err.println("OwnerDAO: Threw a SQLException retrieving the Owner object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    @Override
    public List<IUser> getAllOwners() {
        ArrayList<IUser> owners = new ArrayList<IUser>();
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();

            String selectOwnersQuery = "SELECT * FROM `owner`";

            ResultSet ownerRS = st.executeQuery(selectOwnersQuery);
            System.out.println("OwnerDAO: *************** Query " + selectOwnersQuery);

            while ( ownerRS.next() ) {
                COwner owner = new COwner();
                owner.setSsn(ownerRS.getInt("ssn"));
                owner.setName(ownerRS.getString("name"));
                owners.add(owner);
            }
            //close to manage resources
            ownerRS.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return owners;
        }
        catch( SQLException se ){
            System.err.println("OwnerDAO: Threw a SQLException retrieving the Owner object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

}
