package com.facility.system.dal;

import com.facility.system.model.facmaint.IMaintenanceRequest;
import com.facility.system.model.facoverview.IFacility;

import java.util.List;

/**
 * Created by zherr on 1/26/14.
 * DAO for maintenance requests
 */
public interface IMaintenanceRequestDAO {
    public IMaintenanceRequest getMaintenanceRequest(int id);
    public void addMaintenanceRequest(IMaintenanceRequest maintenanceRequest);
    public List<IMaintenanceRequest> getAllMaintenanceRequest(IFacility facility);
    public void deleteMaintenanceRequest(IMaintenanceRequest maintenanceRequest);
    public void updateMaintenanceRequest(IMaintenanceRequest maintenanceRequest);
    public void addMaintenanceRequestToHistory(IMaintenanceRequest maintenanceRequest);
    public List<IMaintenanceRequest> getAllMaintenanceRequestsFromHistory(IFacility facility);
    public List<IMaintenanceRequest> getAllMaintenanceRequestsCurrent(IFacility facility);
}
