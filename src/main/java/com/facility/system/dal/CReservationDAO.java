package com.facility.system.dal;

import com.facility.system.model.facoperation.CReservation;
import com.facility.system.model.facoperation.IReservation;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.user.CRenter;
import com.facility.system.model.user.IUser;
import com.facility.system.model.utility.TimeStamp;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zherr on 1/27/14.
 */
public class CReservationDAO implements IReservationDAO{
    @Override
    public IReservation getReservation(int reserveId) {
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();
            CRenterDAO renterDAO = new CRenterDAO();

            String selectReservQuery = "SELECT * FROM `reservations` WHERE `reserveId` = '" + reserveId + "'";

            ResultSet reservRS = st.executeQuery(selectReservQuery);
            System.out.println("ReservationDAO: *************** Query " + selectReservQuery);

            CReservation reservation = new CReservation();
            while ( reservRS.next() ) {
                reservation.setReserveId(reserveId);
                int facId = reservRS.getInt("facToReserve");
                CFacilityDAO facDAO = new CFacilityDAO();
                IFacility fac = facDAO.getFacility(facId);
                reservation.setFacToReserve(fac);
                reservation.setNumOfParticipants(reservRS.getInt("numOfParticipants"));
                reservation.setTitle(reservRS.getString("title"));
                TimeStamp start = new TimeStamp(reservRS.getString("Start"));
                assert(null != start);
                TimeStamp end = new TimeStamp(reservRS.getString("End"));
                assert(null != end);
                reservation.setStart(start.toString());
                reservation.setEnd(end.toString());
                IUser renter = renterDAO.getRenter(reservRS.getInt("renterSsn"));
                reservation.setRenter((CRenter) renter);
            }
            //close to manage resources
            reservRS.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return reservation;
        }
        catch( SQLException se ){
            System.err.println("ReservationDAO: Threw a SQLException retrieving the Reservation object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    @Override
    public void addReservation(IReservation reservation) {
        Connection con = DBHelper.getConnection();
        PreparedStatement reservationPst = null;
        PreparedStatement addPst = null;

        try{
            String maintReqStmt = "INSERT INTO `reservations`(facToReserve, reserveId, Start," +
                    " End, title, numOfParticipants, renterSsn) VALUES(?, ?, ?, ?, ?, ?, ?)";
            reservationPst = con.prepareStatement(maintReqStmt);
            reservationPst.setInt(1, reservation.getFacToReserve().getFacilityId());
            reservationPst.setInt(2, reservation.getReserveId());
            reservationPst.setString(3, reservation.getStart().toString());
            reservationPst.setString(4, reservation.getEnd().toString());
            reservationPst.setString(5, reservation.getTitle());
            reservationPst.setInt(6, reservation.getNumOfParticipants());
            reservationPst.setInt(7, reservation.getRenter().getSsn());
            reservationPst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("ReservationDAO: Threw SQLException while adding Reservation to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    reservationPst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("ReservationDAO: Threw a SQLException saving the Reservation object.");
                System.err.println(ex.getMessage());
            }
        }
    }

    @Override
    public List<IReservation> getAllReservations(IFacility facility) {
        ArrayList<IReservation> reserves = new ArrayList<IReservation>();
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();
            CRenterDAO renterDAO = new CRenterDAO();

            String selectReservesQuery = "SELECT * FROM `reservations` WHERE `reservations`.`facToReserve` = " + facility.getFacilityId();

            ResultSet reservRS = st.executeQuery(selectReservesQuery);
            System.out.println("ReservationDAO: *************** Query " + selectReservesQuery);

            while ( reservRS.next() ) {
                CReservation reservation = new CReservation();
                reservation.setReserveId(reservRS.getInt("reserveId"));
                int facId = reservRS.getInt("facToReserve");
                CFacilityDAO facDAO = new CFacilityDAO();
                IFacility fac = facDAO.getFacility(facId);
                reservation.setFacToReserve(fac);
                reservation.setNumOfParticipants(reservRS.getInt("numOfParticipants"));
                reservation.setTitle(reservRS.getString("title"));
                TimeStamp start = new TimeStamp(reservRS.getString("Start"));
                assert(null != start);
                TimeStamp end = new TimeStamp(reservRS.getString("End"));
                assert(null != end);
                reservation.setStart(start.toString());
                reservation.setEnd(end.toString());
                IUser renter = renterDAO.getRenter(reservRS.getInt("renterSsn"));
                reservation.setRenter((CRenter) renter);
                reserves.add(reservation);
            }
            //close to manage resources
            reservRS.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return reserves;
        }
        catch( SQLException se ){
            System.err.println("ReservationDAO: Threw a SQLException retrieving the Reservation object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteReservation(IReservation reservation) {
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();
            String deleteSql = "DELETE FROM `reservations` WHERE `reserveId`= " + reservation.getReserveId() + ";";
            int result = st.executeUpdate(deleteSql);
            st.close();
        } catch( SQLException e ){
            System.err.println("ReservationDAO: Threw exception when trying to delete a Reservation");
            e.printStackTrace();
        }
    }

    @Override
    public void updateReservation(IReservation reservation) {
        // Empty
    }

    @Override
    public void deleteAllReservations(IFacility facility) {
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();
            String deleteSql = "DELETE FROM `reservations` WHERE `facToReserve`= " + facility.getFacilityId() + ";";
            int result = st.executeUpdate(deleteSql);
            st.close();
            con.close();
        } catch( SQLException e ){
            System.err.println("ReservationDAO: Threw exception when trying to delete a Reservation");
            e.printStackTrace();
        }
    }
}
