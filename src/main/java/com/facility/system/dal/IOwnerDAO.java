package com.facility.system.dal;

import com.facility.system.model.user.IUser;

import java.util.List;

/**
 * Created by zherr on 1/26/14.
 * DAO for facilities
 */
public interface IOwnerDAO {

    public void addOwner(IUser user);
    public void deleteOwner(IUser user);
    public void updateOwner(IUser user);
    public IUser getOwner(int ssn);
    public List<IUser> getAllOwners();

}
