package com.facility.system.dal;

import com.facility.system.model.facoverview.CGeneralFacility;
import com.facility.system.model.facoverview.IFacility;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zherr on 1/27/14.
 */
public class CFacilityDAO implements IFacilityDAO{

    public CFacilityDAO() {}

    @Override
    public IFacility getFacility(int facId) {
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();

            String selectFacilityQuery = "SELECT * FROM `facility` WHERE `FacNumber` = '" + facId + "'";

            ResultSet facilityRS = st.executeQuery(selectFacilityQuery);
            System.out.println("FacilityDAO: *************** Query " + selectFacilityQuery);

            CGeneralFacility facility = new CGeneralFacility();
            while ( facilityRS.next() ) {
                facility.setFacilityId(facId);
                facility.setAvailableCapacity(facilityRS.getInt("availableCap"));
                facility.setTotalCapacity(facilityRS.getInt("totalCap"));
                facility.setFacilityDescription(facilityRS.getString("Description"));
                facility.setFacilityLocation(facilityRS.getString("Location"));
                facility.setFacilityName(facilityRS.getString("Name"));
            }
            //close to manage resources
            facilityRS.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return facility;
        }
        catch( SQLException se ){
            System.err.println("FacilityDAO: Threw a SQLException retrieving the Facility object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    @Override
    public void addFacility(IFacility fac) {
        Connection con = DBHelper.getConnection();
        PreparedStatement facilityPst = null;
        PreparedStatement addPst = null;

        try{
            String facilityStmt = "INSERT INTO `facility`(FacNumber, Name, Description, Location, totalCap, availableCap) VALUES(?, ?, ?, ?, ?, ?)";
            facilityPst = con.prepareStatement(facilityStmt);
            facilityPst.setInt(1, fac.getFacilityId());
            facilityPst.setString(2, fac.getFacilityName());
            facilityPst.setString(3, fac.getFacilityDescription());
            facilityPst.setString(4, fac.getFacilityLocation());
            facilityPst.setInt(5, fac.getTotalCapacity());
            facilityPst.setInt(6, fac.getAvailableCapacity());
            facilityPst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("FacilityDAO: Threw SQLException while adding Facility to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (addPst != null) {
                    addPst.close();
                    facilityPst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("Facility: Threw a SQLException saving the Facility object.");
                System.err.println(ex.getMessage());
            }
        }
    }

    @Override
    public List<IFacility> getAllFacilities() {
        ArrayList<IFacility> facilities = new ArrayList<IFacility>();
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();

            String selectFacilitiesQuery = "SELECT * FROM `facility`";

            ResultSet facilitiesRS = st.executeQuery(selectFacilitiesQuery);
            System.out.println("FacilityDAO: *************** Query " + selectFacilitiesQuery);

            while ( facilitiesRS.next() ) {
                CGeneralFacility facility = new CGeneralFacility();
                facility.setFacilityId(facilitiesRS.getInt("FacNumber"));
                facility.setAvailableCapacity(facilitiesRS.getInt("availableCap"));
                facility.setTotalCapacity(facilitiesRS.getInt("totalCap"));
                facility.setFacilityDescription(facilitiesRS.getString("Description"));
                facility.setFacilityLocation(facilitiesRS.getString("Location"));
                facility.setFacilityName(facilitiesRS.getString("Name"));
                facilities.add(facility);
            }
            //close to manage resources
            facilitiesRS.close();
            st.close();
            con.close();
            DBHelper.closeConnection();

            return facilities;
        }
        catch( SQLException se ){
            System.err.println("FacilityDAO: Threw a SQLException retrieving the Facility object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteFacility(IFacility fac) {
        try{
            Connection con = DBHelper.getConnection();
            Statement st = con.createStatement();
            String deleteSql = "DELETE FROM `facility` WHERE `FacNumber`= " + fac.getFacilityId() + ";";
            int result = st.executeUpdate(deleteSql);
            st.close();
            con.close();
        } catch( SQLException e ){
            System.err.println("FacilityDAO: Threw exception when trying to delete a facility");
            e.printStackTrace();
        }
    }

    @Override
    public void updateFacility(IFacility fac) {
        Connection con = DBHelper.getConnection();
        PreparedStatement facilityPst = null;
        PreparedStatement updatePst = null;

        try{
            String facSql = "UPDATE `facility` SET `Name` = ?, `Description` = ?, `Location` = ?, `totalCap` = ?, `availableCap` = ? WHERE `FacNumber` = " + fac.getFacilityId();
            facilityPst = con.prepareStatement(facSql);
            facilityPst.setString(1, fac.getFacilityName());
            facilityPst.setString(2, fac.getFacilityDescription());
            facilityPst.setString(3, fac.getFacilityLocation());
            facilityPst.setInt(4, fac.getTotalCapacity());
            facilityPst.setInt(5, fac.getAvailableCapacity());
            facilityPst.executeUpdate();
        }
        catch(SQLException se){
            System.err.println("FacDao: Threw SQLException while updating Facility to database.");
            System.err.println(se.getMessage());
        }
        finally{

            try {
                if (updatePst != null) {
                    updatePst.close();
                    facilityPst.close();
                }
                if (con != null) {
                    con.close();
                }
                DBHelper.closeConnection();

            } catch (SQLException ex) {
                System.err.println("FacDAO: Threw a SQLException updating the Fac object.");
                System.err.println(ex.getMessage());
            }
        }
    }
}
