package com.facility.system.client;

import com.facility.system.dal.hibernate.*;
import com.facility.system.model.facmaint.CFacilityMaintainer;
import com.facility.system.model.facmaint.CGeneralMaintenance;
import com.facility.system.model.facmaint.CMaintenanceRequest;
import com.facility.system.model.facmaint.IMaintenance;
import com.facility.system.model.facoperation.*;
import com.facility.system.model.facoverview.CFacilityOverseer;
import com.facility.system.model.facoverview.COwnedFacility;
import com.facility.system.model.user.COwner;
import com.facility.system.model.user.CRenter;
import com.facility.system.model.userservices.COwnerService;
import com.facility.system.model.userservices.CRenterService;
import com.facility.system.model.utility.Time;
import com.facility.system.model.utility.TimeStamp;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class App extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        try {
        resp.getWriter().println("Welcome to the Facility Management System!\n");
        resp.getWriter().println("...:::::Testing the system:::::...\n");
        ApplicationContext appContext = new ClassPathXmlApplicationContext("META-INF/app-context.xml");
        resp.getWriter().println(".....::::::::::Application context instantiated!::::::::::.....\n");
        // stub time stamps
        Time springTime = (Time) appContext.getBean("time");
        springTime.setHours(15);
        springTime.setMinutes(30);
        springTime.setSeconds(0);
        springTime.setmSecs(0);
        TimeStamp t1 = (TimeStamp) appContext.getBean("timeStamp");
        t1.setMonth(2);
        t1.setDay(1);
        t1.setYear(2014);
        t1.setTime(springTime);
        TimeStamp t2 = (TimeStamp) appContext.getBean("timeStamp");
        t2.setMonth(2);
        t2.setDay(2);
        t2.setYear(2014);
        t2.setTime(springTime);
        TimeStamp t3 = (TimeStamp) appContext.getBean("timeStamp");
        t3.setMonth(3);
        t3.setDay(6);
        t3.setYear(2014);
        t3.setTime(springTime);
        TimeStamp t4 = (TimeStamp) appContext.getBean("timeStamp");
        t4.setMonth(3);
        t4.setDay(7);
        t4.setYear(2014);
        t4.setTime(springTime);
        // reservation DAO for adding reservations back in after vacating
        HibernateReservationDAO springResDAO = (HibernateReservationDAO) appContext.getBean("reserveDAO");
        HibernateMaintenanceRequestDAO springMaintDAO = (HibernateMaintenanceRequestDAO) appContext.getBean("maintReqDAO");
        HibernateFacilityDAO sOwnedFacDAO = (HibernateFacilityDAO) appContext.getBean("ownFacDAO");
        HibernateMaintenanceDAO sMaintDAO = (HibernateMaintenanceDAO) appContext.getBean("maintDAO");
        // Users
        CRenter renter0 = (CRenter) appContext.getBean("renter");
        renter0.setSsn(566778899);
        renter0.setName("testRenter");
        COwner owner0 = (COwner) appContext.getBean("owner");
        owner0.setSsn(123456789);
        owner0.setName("testOwner");
        //
        resp.getWriter().println("Welcome to the Facility Management System!\n");
        resp.getWriter().println("...:::::Testing the system:::::...\n");
        //

        // Spring Facility Overseer
        CFacilityOverseer springFacOverseer = (CFacilityOverseer) appContext.getBean("facilityOverseer");
        springFacOverseer.setFacilityDAO((HibernateFacilityDAO)appContext.getBean("ownFacDAO"));

        // Spring Facility Operator
        CFacilityOperator springFacilityOperator = (CFacilityOperator) appContext.getBean("facilityOperator");
        CFacilityReserver springReserver = (CFacilityReserver) appContext.getBean("facilityReserver");
        HibernateInspectionDAO springInspectDAO = (HibernateInspectionDAO) appContext.getBean("inspectDAO");
        springReserver.setReservationDAO(springResDAO);
        springFacilityOperator.setFacilityReserver(springReserver);
        springFacilityOperator.setInspectionDAO(springInspectDAO);

        // Spring Facility Maintainer
        CFacilityMaintainer facilityMaintainer = (CFacilityMaintainer) appContext.getBean("facilityMaintainer");

        // User services
        resp.getWriter().println("\n..:::::Testing User Services::::..\n");
        HibernateOwnerDAO sOwnerDAO = (HibernateOwnerDAO) appContext.getBean("ownerDAO");
        HibernateRenterDAO sRenterDAO = (HibernateRenterDAO) appContext.getBean("renterDAO");
        COwner sOwner = (COwner) sOwnerDAO.getOwner(123456789);
        CRenter sRenter = (CRenter) sRenterDAO.getRenter(566778899);
        CRenter sRenter2 = (CRenter) sRenterDAO.getRenter(112233445);
        COwnerService springOwnerService = (COwnerService) appContext.getBean("ownerService");
        springOwnerService.setFacilityMaintainer(facilityMaintainer);
        springOwnerService.setFacilityOperator(springFacilityOperator);
        springOwnerService.setFacilityOverseer(springFacOverseer);
        CRenterService springRenterService = (CRenterService) appContext.getBean("renterService");
        springRenterService.setFacilityReserver(springReserver);
        springRenterService.setFacilityOperator(springFacilityOperator);
        springRenterService.setFacilityOverseer(springFacOverseer);

        resp.getWriter().println("Owner Service:\n");
        COwnedFacility sOwnedFac = (COwnedFacility) sOwnedFacDAO.getFacility(0);
        resp.getWriter().println("Is facility 0 owned by " + sOwner.getName() + " ?...."
                + springOwnerService.checkIfFacOwnedByOwner(sOwnedFac, sOwner));
        List<IMaintenance> maintsForOwner = new ArrayList<IMaintenance>();
        maintsForOwner = springOwnerService.listOwnMaintenance(sOwnedFac, sOwner);
        resp.getWriter().println("Listing maintenances for Owner");
        for(IMaintenance maint : maintsForOwner) {
            resp.getWriter().println("Maint: " + maint.getMaintenanceDescription());
        }
        resp.getWriter().println("Scheduling maintenance through owner");

        CGeneralMaintenance maint0 = (CGeneralMaintenance) appContext.getBean("maintenance");
        maint0 = (CGeneralMaintenance) sMaintDAO.getMaintenance(1);

        CMaintenanceRequest maintReq = (CMaintenanceRequest) appContext.getBean("maintRequest");
        maintReq.setFacilityRequested(sOwnedFac);
        maintReq.setMaintReqId(55);
        maintReq.setIsDone(false);
        maintReq.setMaintenance(maint0);
        maintReq.setTimeRequested(t1.toString());

        springOwnerService.makeMaintenanceRequest(maintReq, sOwner);
        resp.getWriter().println("Removing that maintenance");
        springMaintDAO.deleteMaintenanceRequest(maintReq);
        resp.getWriter().println("Listing all inspection by Owner");
        List<IInspection> sInspections = new ArrayList<IInspection>();
        sInspections = springOwnerService.seeOwnFacilityInspections(sOwnedFac, sOwner);
        for(IInspection ins : sInspections) {
            resp.getWriter().println("Inspection: " + ins.getInspectionName());
        }
        resp.getWriter().println("Calculating " + sOwner.getName() + "'s " + sOwnedFac.getFacilityName() + "facility's maint cost: " +
                                    springOwnerService.calcMaintCostForOwnFacility(sOwnedFac, sOwner));
        resp.getWriter().println("Calculating " + sOwner.getName() + "'s " + sOwnedFac.getFacilityName() + "facility's down time: " +
                springOwnerService.calcDownTimeForOwnFacility(sOwnedFac, sOwner) + " hours");
        resp.getWriter().println("Calculating " + sOwner.getName() + "'s " + sOwnedFac.getFacilityName() + "facility's usage rate: " +
                springOwnerService.getOwnUsageRate(sOwnedFac, sOwner));

        resp.getWriter().println("Adding facility detail through owner for facility: " + sOwnedFac.getFacilityName() + " - " + sOwnedFac.getFacilityDescription());
        String oldOFacDescr = sOwnedFac.getFacilityDescription();
        sOwnedFac.setFacilityDescription("NEW DESCRIPTION");
        springOwnerService.addOwnFacilityDetail(sOwnedFac, owner0);
        resp.getWriter().println("New description: " + ((COwnedFacility) sOwnedFacDAO.getFacility(0)).getFacilityDescription());
        sOwnedFac.setFacilityDescription(oldOFacDescr);
        springOwnerService.addOwnFacilityDetail(sOwnedFac, owner0);

        List<IReservation> reserves = new ArrayList<IReservation>();
        reserves = springOwnerService.seeOwnFacilityReservations(sOwnedFac, owner0);
        resp.getWriter().println("Listing own reservations for: " + sOwner.getName());
        for(IReservation reserve : reserves)
        {
            resp.getWriter().println("Reservation: " + reserve.getReserveId() + " : " + reserve.getTitle());
        }
        //

        resp.getWriter().println("\nRenter Service:\n");
        CReservation springReservation = (CReservation) springResDAO.getReservation(0);
        resp.getWriter().println("Is reservation 0 owned by " + sRenter.getName() + " ?...."
                + springRenterService.checkIfReservedByRenter(springReservation, sRenter));
        resp.getWriter().println("Is reservation 1 owned by " + sRenter2.getName() + " ?...."
                + springRenterService.checkIfReservedByRenter(springReservation, sRenter2));
        resp.getWriter().println("Renter making reservation");
        CReservation res2 = (CReservation) appContext.getBean("reservation");
        res2.setReserveId(50);
        res2.setFacToReserve(sOwnedFac);
        res2.setStart(t1.toString());
        res2.setEnd(t2.toString());
        res2.setTitle("Training");
        res2.setNumOfParticipants(50);
        res2.setRenter(sRenter);
        springRenterService.makeReservation(res2, sRenter);
        resp.getWriter().println("Renter removing reservation");
        springRenterService.removeReservation(res2, sRenter);

        resp.getWriter().println("Renter finding available capacity of facility: " + springRenterService.getAvailableCapacity(sOwnedFac));
        resp.getWriter().println("Renter getting facility info: " + springRenterService.getFacilityInfo(sOwnedFac));
        resp.getWriter().println("Renter finding out if facility is in use during certain interval: " + t1.toString() + " - " + t2.toString() + "............. " +
                                    springRenterService.checkIfFacilityInUse(sOwnedFac, t1, t2));
        resp.getWriter().println("Renter finding out if facility is in use during certain interval: "+ t3.toString() + " - " + t4.toString() + "............. " +
                springRenterService.checkIfFacilityInUse(sOwnedFac, t3, t4));
        } catch(NullPointerException exception) {
            resp.getWriter().println(exception.toString());
            exception.printStackTrace(resp.getWriter());
            resp.getWriter().println("\n\nIf you are seeing this error, the application is trying to 'catch up' with" +
                    " itself. This is most likely due to the slow slow response time of the server. This only happens" +
                    "in deployment. Please consult Zach for details, but in the meantime...\n\n Refresh the page in a" +
                    " few seconds and it typically works.");
        }
    }
    public static void main(String[] args) throws Exception{
        Server server = new Server(Integer.valueOf(System.getenv("PORT")));
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
        context.addServlet(new ServletHolder(new App()),"/*");
        server.start();
        server.join();
    }
}
