package com.facility.system.model.userservices;

import com.facility.system.model.facmaint.IFacilityMaintainer;
import com.facility.system.model.facmaint.IMaintenance;
import com.facility.system.model.facmaint.IMaintenanceRequest;
import com.facility.system.model.facoperation.IFacilityOperator;
import com.facility.system.model.facoperation.IInspection;
import com.facility.system.model.facoperation.IReservation;
import com.facility.system.model.facoverview.COwnedFacility;
import com.facility.system.model.facoverview.IFacilityOverseer;
import com.facility.system.model.user.COwner;

import java.util.List;

/**
 * Created by zherr on 2/21/14.
 * @Note The Owner service may have a limited view over the interfaces it wraps around. See implementation
 * for details.
 */
public interface IOwnerService {
    public List<IInspection> seeOwnFacilityInspections(COwnedFacility ownedFacility, COwner owner);
    public void makeMaintenanceRequest(IMaintenanceRequest maintReq, COwner owner);
    public List<IMaintenance> listOwnMaintenance(COwnedFacility ownedFacility, COwner owner);
    public boolean checkIfFacOwnedByOwner(COwnedFacility fac, COwner owner);
    public float calcMaintCostForOwnFacility(COwnedFacility fac, COwner owner);
    public int calcDownTimeForOwnFacility(COwnedFacility fac, COwner owner);
    public void vacateOwnFacility(COwnedFacility fac, COwner owner);
    public List<IReservation> seeOwnFacilityReservations(COwnedFacility fac, COwner owner);
    public int getOwnUsageRate(COwnedFacility fac, COwner owner);
    public void addOwnFacilityDetail(COwnedFacility fac, COwner owner);
    public void setFacilityMaintainer(IFacilityMaintainer maintainer);
    public void setFacilityOperator(IFacilityOperator operator);
    public void setFacilityOverseer(IFacilityOverseer overseer);
}
