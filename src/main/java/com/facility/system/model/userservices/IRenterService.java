package com.facility.system.model.userservices;

import com.facility.system.model.facoperation.IFacilityOperator;
import com.facility.system.model.facoperation.IFacilityReserver;
import com.facility.system.model.facoperation.IReservation;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.facoverview.IFacilityOverseer;
import com.facility.system.model.user.CRenter;
import com.facility.system.model.utility.TimeStamp;

/**
 * Created by zherr on 2/21/14.
 * @Note The Renter Service may have a limited view over the interfaces it wraps around. See implementation
 * for details.
 */
public interface IRenterService {
    public boolean makeReservation(IReservation reservation, CRenter renter);
    public boolean removeReservation(IReservation reservation, CRenter renter);
    public boolean checkIfReservedByRenter(IReservation reservation, CRenter renter);
    public boolean checkIfFacilityInUse(IFacility fac, TimeStamp start, TimeStamp end);
    public int getAvailableCapacity(IFacility fac);
    public String getFacilityInfo(IFacility fac);
    public void setFacilityReserver(IFacilityReserver facReserver);
    public void setFacilityOperator(IFacilityOperator operator);
    public void setFacilityOverseer(IFacilityOverseer overseer);
}
