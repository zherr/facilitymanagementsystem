package com.facility.system.model.userservices;

import com.facility.system.model.facoperation.IFacilityOperator;
import com.facility.system.model.facoperation.IFacilityReserver;
import com.facility.system.model.facoperation.IReservation;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.facoverview.IFacilityOverseer;
import com.facility.system.model.user.CRenter;
import com.facility.system.model.utility.TimeStamp;

/**
 * Created by Zach on 2/22/14.
 * A service for renters to use.
 */
public class CRenterService implements IRenterService {

    private IFacilityReserver facilityReserver;
    private IFacilityOverseer facilityOverseer;
    private IFacilityOperator facilityOperator;

    public CRenterService() {}

    public CRenterService(IFacilityReserver facilityReserver) {
        this.facilityReserver = facilityReserver;
    }

    @Override
    public boolean makeReservation(IReservation reservation, CRenter renter) {
        assert(checkIfReservedByRenter(reservation, renter));
        facilityReserver.addReservation(reservation);
        return true;
    }

    @Override
    public boolean removeReservation(IReservation reservation, CRenter renter) {
        assert(checkIfReservedByRenter(reservation, renter));
        facilityReserver.removeReservation(reservation);
        return true;
    }

    @Override
    public boolean checkIfReservedByRenter(IReservation reservation, CRenter renter) {
        return reservation.getRenter().getSsn() == renter.getSsn() ? true : false;
    }

    @Override
    public boolean checkIfFacilityInUse(IFacility fac, TimeStamp start, TimeStamp end) {
        return facilityOperator.IsInUseDuringInterval(fac, start, end);
    }

    @Override
    public int getAvailableCapacity(IFacility fac) {
        return facilityOverseer.requestAvailableCapacity(fac);
    }

    @Override
    public String getFacilityInfo(IFacility fac) {
        return facilityOverseer.getFacilityInformation(fac);
    }

    @Override
    public void setFacilityOperator(IFacilityOperator operator) {
        this.facilityOperator = operator;
    }

    @Override
    public void setFacilityOverseer(IFacilityOverseer overseer) {
        this.facilityOverseer = overseer;
    }

    @Override
    public void setFacilityReserver(IFacilityReserver facReserver) {
        this.facilityReserver = facReserver;
    }
}
