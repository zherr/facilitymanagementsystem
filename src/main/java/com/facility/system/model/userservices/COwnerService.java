package com.facility.system.model.userservices;

import com.facility.system.model.facmaint.IFacilityMaintainer;
import com.facility.system.model.facmaint.IMaintenance;
import com.facility.system.model.facmaint.IMaintenanceRequest;
import com.facility.system.model.facoperation.IFacilityOperator;
import com.facility.system.model.facoperation.IInspection;
import com.facility.system.model.facoperation.IReservation;
import com.facility.system.model.facoverview.COwnedFacility;
import com.facility.system.model.facoverview.IFacilityOverseer;
import com.facility.system.model.user.COwner;

import java.util.List;

/**
 * Created by Zach on 2/22/14.
 * A service for owners to use.
 */
public class COwnerService implements IOwnerService {

    private IFacilityMaintainer facilityMaintainer;
    private IFacilityOperator facilityOperator;
    private IFacilityOverseer facilityOverseer;

    public COwnerService() {}

    public COwnerService(IFacilityMaintainer facilityMaintainer, IFacilityOperator facilityOperator, IFacilityOverseer facilityOverseer) {
        this.facilityMaintainer = facilityMaintainer;
        this.facilityOperator = facilityOperator;
        this.facilityOverseer = facilityOverseer;
    }

    @Override
    public List<IInspection> seeOwnFacilityInspections(COwnedFacility ownedFacility, COwner owner) {
        assert(checkIfFacOwnedByOwner(ownedFacility, owner));
        return facilityOperator.listInspections(ownedFacility);
    }

    @Override
    public void makeMaintenanceRequest(IMaintenanceRequest maintReq, COwner owner) {
        assert(checkIfFacOwnedByOwner((COwnedFacility)maintReq.getFacilityRequested(), owner));
        facilityMaintainer.makeFacilityMaintRequest(maintReq);
    }

    @Override
    public List<IMaintenance> listOwnMaintenance(COwnedFacility ownedFacility, COwner owner) {
        assert(checkIfFacOwnedByOwner(ownedFacility, owner));
        return facilityMaintainer.listMaintenance(ownedFacility);
    }

    @Override
    public void setFacilityMaintainer(IFacilityMaintainer maintainer) {
        this.facilityMaintainer = maintainer;
    }

    @Override
    public float calcMaintCostForOwnFacility(COwnedFacility fac, COwner owner) {
        assert(checkIfFacOwnedByOwner(fac, owner));
        return facilityMaintainer.calcMaintenanceCostForFacility(fac);
    }

    @Override
    public int calcDownTimeForOwnFacility(COwnedFacility fac, COwner owner) {
        assert(checkIfFacOwnedByOwner(fac, owner));
        return facilityMaintainer.calcDownTimeForFacility(fac);
    }

    @Override
    public void vacateOwnFacility(COwnedFacility fac, COwner owner) {
        assert(checkIfFacOwnedByOwner(fac, owner));
        facilityOperator.vacateFacility(fac);
    }

    @Override
    public List<IReservation> seeOwnFacilityReservations(COwnedFacility fac, COwner owner) {
        assert(checkIfFacOwnedByOwner(fac, owner));
        return facilityOperator.listReservations(fac);
    }

    @Override
    public int getOwnUsageRate(COwnedFacility fac, COwner owner) {
        assert(checkIfFacOwnedByOwner(fac, owner));
        return facilityOperator.calcUsageRate(fac);
    }

    @Override
    public void setFacilityOperator(IFacilityOperator operator) {
        this.facilityOperator = operator;
    }

    @Override
    public void setFacilityOverseer(IFacilityOverseer overseer) {
        this.facilityOverseer = overseer;
    }

    @Override
    public void addOwnFacilityDetail(COwnedFacility fac, COwner owner) {
        assert(checkIfFacOwnedByOwner(fac, owner));
        facilityOverseer.addFacilityDetail(fac.getFacilityId(), fac.getFacilityName(), fac.getFacilityDescription(), fac.getFacilityLocation(),
                fac.getAvailableCapacity(), fac.getTotalCapacity(), fac.getOwner());
    }

    @Override
    public boolean checkIfFacOwnedByOwner(COwnedFacility fac, COwner owner) {
        return fac.getOwner().getSsn() == owner.getSsn() ? true : false;
    }
}
