package com.facility.system.model.user;

/**
 * Created by zherr on 2/21/14.
 */
public abstract class AUser implements IUser {

    protected int ssn;
    protected String name;

    protected AUser() {}

    protected AUser(int ssn, String name) {
        this.ssn = ssn;
        this.name = name;
    }

    @Override
    public int getSsn() {
        return this.ssn;
    }

    @Override
    public void setSsn(int ssn) {
        this.ssn = ssn;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
