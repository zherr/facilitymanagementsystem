package com.facility.system.model.user;

/**
 * Created by zherr on 2/21/14.
 */
public class COwner extends AUser {

    public COwner() {}

    public COwner(int ssn, String name) {
        super(ssn, name);
    }
}
