package com.facility.system.model.user;

/**
 * Created by zherr on 2/21/14.
 * TODO tests
 */
public interface IUser {
    public int getSsn();
    public void setSsn(int ssn);
    public String getName();
    public void setName(String name);
}
