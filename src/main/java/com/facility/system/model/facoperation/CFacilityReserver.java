package com.facility.system.model.facoperation;

import com.facility.system.dal.IReservationDAO;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.utility.TimeStamp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zherr on 1/27/14.
 */
public class CFacilityReserver extends AFacilityReserver{

    public CFacilityReserver() {}

    public CFacilityReserver(IReservationDAO reservationDAO, List<IReservation> reservations){
        super(reservationDAO, reservations);
    }

    /**
     * Gets the rate a facility has been used
     *
     * @param facility - The facility for calculate
     * @return The number of reservations for a facility
     */
    @Override
    public int usageRate(IFacility facility) {
        List<IReservation> reservations = new ArrayList<IReservation>();
        reservations = this.reservationDAO.getAllReservations(facility);
        return reservations.size();
    }

    /**
     * If the date ranges overlap, the facility is in use
     *
     * @param facility
     * @param start
     * @param end
     * @return
     */
    @Override
    public boolean inUse(IFacility facility, TimeStamp start, TimeStamp end) {
        List<IReservation> reservations = new ArrayList<IReservation>();
        reservations = this.reservationDAO.getAllReservations(facility);
        for(IReservation reserve : reservations){
            System.out.println(reserve.getStart() + " " + reserve.getEnd());
            System.out.println(start + " " + end);
            TimeStamp rStart = new TimeStamp(reserve.getStart());
            TimeStamp rEnd = new TimeStamp(reserve.getEnd());
            // If the years and months overlap
            if(rStart.getYear() <= end.getYear() && start.getYear() <= rEnd.getYear() &&
                    rStart.getMonth() <= end.getMonth() && start.getMonth() <= rEnd.getMonth()) {
//                    reserve.getStart().getDay() <= getEnd.getDay() && getStart.getDay() <= reserve.getEnd().getDay()) {
                if(rStart.getMonth() == end.getMonth() && start.getMonth() == rEnd.getMonth() &&
                        ((rStart.getDay() <= end.getDay()) && (start.getDay() >= rEnd.getDay()) ||
                        start.getDay() <= rEnd.getDay() && rStart.getDay() >= end.getDay())) {
                    return false;
                }
                return true;
            }
//            // Else if the year and months are the same, check if the days overlap
//            else if(reserve.getStart().getYear() <= getEnd.getYear() && getStart.getYear() <= reserve.getEnd().getYear() &&
//                    reserve.getStart().getMonth() <= getEnd.getMonth() && getStart.getMonth() <= reserve.getEnd().getMonth()) {
//                return true;
//            }
        }
        return false;
    }
}
