package com.facility.system.model.facoperation;

import com.facility.system.dal.IReservationDAO;
import com.facility.system.model.facoverview.IFacility;

import java.util.List;

/**
 * Created by zherr on 1/27/14.
 */
public abstract class AFacilityReserver implements IFacilityReserver{
    protected List<IReservation> reservations;
    protected IReservationDAO reservationDAO;

    protected AFacilityReserver() {}

    protected AFacilityReserver(IReservationDAO reservationDAO, List<IReservation> reservations) {
        this.reservationDAO = reservationDAO;
        this.reservations = reservations;
    }

    @Override
    public void addReservation(IReservation reservation) {
        reservations.add(reservation);
        reservationDAO.addReservation(reservation);
    }

    @Override
    public void removeReservation(IReservation reservation) {
        reservations.remove(reservation);
        reservationDAO.deleteReservation(reservation);
    }

    @Override
    public void removeAllReservations(IFacility facility) {
        reservations.clear();
        reservationDAO.deleteAllReservations(facility);
    }

    @Override
    public List<IReservation> listAllReservations(IFacility facility) {
        return reservationDAO.getAllReservations(facility);
    }

    @Override
    public void setReservations(List<IReservation> reservations) {
        this.reservations = reservations;
    }

    @Override
    public IReservationDAO getReservationDAO() {
        return this.reservationDAO;
    }

    @Override
    public void setReservationDAO(IReservationDAO reservationDAO) {
        this.reservationDAO = reservationDAO;
    }

    @Override
    public List<IReservation> getReservations() {
        return this.reservations;
    }
}
