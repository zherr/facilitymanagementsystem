package com.facility.system.model.facoperation;

import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.user.CRenter;

/**
 * Created by zherr on 1/27/14.
 */
public class CReservation implements IReservation{
    private int reserveId;
    private IFacility facToReserve;
    private String start;
    private String end;
    private String title;
    private int numOfParticipants;
    private CRenter renter;

    public CReservation() {}

    /**
     * Constructor
     * @param reserveId
     * @param facToReserve
     * @param start
     * @param end
     * @param title
     * @param numOfParticipants
     * @param renter
     */
    public CReservation(int reserveId, IFacility facToReserve, String start, String end, String title, int numOfParticipants, CRenter renter) {
        this.reserveId = reserveId;
        this.facToReserve = facToReserve;
        this.start = start;
        this.end = end;
        this.title = title;
        this.numOfParticipants = numOfParticipants;
        this.renter = renter;
    }

    @Override
    public int getReserveId() {
        return this.reserveId;
    }

    @Override
    public IFacility getFacToReserve() {
        return this.facToReserve;
    }

    @Override
    public String getStart() {
        return this.start;
    }

    @Override
    public String getEnd() {
        return this.end;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public int getNumOfParticipants() {
        return this.numOfParticipants;
    }

    @Override
    public void setReserveId(int id) {
        this.reserveId = id;
    }

    @Override
    public void setFacToReserve(IFacility fac) {
        this.facToReserve = fac;
    }

    @Override
    public void setStart(String start) {
        this.start = start;
    }

    @Override
    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void setNumOfParticipants(int participants) {
        this.numOfParticipants = participants;
    }

    public CRenter getRenter() {
        return renter;
    }

    public void setRenter(CRenter renter) {
        this.renter = renter;
    }
}
