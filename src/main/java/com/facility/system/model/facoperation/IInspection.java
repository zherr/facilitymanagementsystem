package com.facility.system.model.facoperation;

import com.facility.system.model.facoverview.IFacility;

/**
 * Created by zherr on 1/27/14.
 */
public interface IInspection {
    public int getInspectId();
    public String getInspectionName();
    public IFacility getInspectFacility();
    public void setInspectId(int id);
    public void setInspectionName(String name);
    public void setInspectFacility(IFacility facility);
}
