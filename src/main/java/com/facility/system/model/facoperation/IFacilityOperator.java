package com.facility.system.model.facoperation;

import com.facility.system.dal.IInspectionDAO;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.utility.TimeStamp;

import java.util.List;

/**
 * Created by zherr on 1/27/14.
 * Interface for larger facility operator. Responsible for making reservations and querying for other information
 */
public interface IFacilityOperator {
    public boolean IsInUseDuringInterval(IFacility facility, TimeStamp start, TimeStamp end);
    public void assignFacilityToUse(IReservation reservation);
    public void vacateFacility(IFacility facility);
    public List<IInspection> listInspections(IFacility facility);
    public List<IReservation> listReservations(IFacility facility);
    public int calcUsageRate(IFacility facility);
    public void setInspectionDAO(IInspectionDAO inspectionDAO);
    public IInspectionDAO getInspectionDAO();
    public void setFacilityReserver(IFacilityReserver reserver);
    public IFacilityReserver getFacilityReserver();
}
