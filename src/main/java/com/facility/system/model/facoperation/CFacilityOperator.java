package com.facility.system.model.facoperation;

import com.facility.system.dal.IInspectionDAO;

/**
 * Created by zherr on 1/27/14.
 */
public class CFacilityOperator extends AFacilityOperator {

    public CFacilityOperator() {}

    public CFacilityOperator(IFacilityReserver facilityReserver, IInspectionDAO inspectionDAO) {
        super(facilityReserver, inspectionDAO);
    }
}
