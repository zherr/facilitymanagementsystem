package com.facility.system.model.facoperation;

import com.facility.system.dal.IReservationDAO;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.utility.TimeStamp;

import java.util.List;

/**
 * Created by zherr on 1/27/14.
 * Interface for facility overseer's. Generally responsible for making reservations.
 */
public interface IFacilityReserver {
    public void addReservation(IReservation reservation);
    public void removeReservation(IReservation reservation);
    public void removeAllReservations(IFacility facility);
    public int usageRate(IFacility facility);
    public boolean inUse(IFacility facility, TimeStamp start, TimeStamp end);
    public List<IReservation> listAllReservations(IFacility facility);
    public void setReservations(List<IReservation> reservations);
    public List<IReservation> getReservations();
    public IReservationDAO getReservationDAO();
    public void setReservationDAO(IReservationDAO reservationDAO);
}
