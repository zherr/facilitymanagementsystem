package com.facility.system.model.facoperation;

import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.user.CRenter;

/**
 * Created by zherr on 1/27/14.
 */
public interface IReservation {
    public int getReserveId();
    public IFacility getFacToReserve();
    public String getStart();
    public String getEnd();
    public String getTitle();
    public int getNumOfParticipants();
    public void setReserveId(int id);
    public void setFacToReserve(IFacility fac);
    public void setStart(String time);
    public void setEnd(String time);
    public void setTitle(String title);
    public void setNumOfParticipants(int participants);
    public void setRenter(CRenter renter);
    public CRenter getRenter();
}
