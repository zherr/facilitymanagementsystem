package com.facility.system.model.facoperation;

import com.facility.system.dal.IInspectionDAO;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.utility.TimeStamp;

import java.util.List;

/**
 * Created by zherr on 1/27/14.
 */
public class AFacilityOperator implements IFacilityOperator{

    protected IFacilityReserver facilityReserver;
    protected IInspectionDAO inspectionDAO;

    protected AFacilityOperator() {}

    protected AFacilityOperator(IFacilityReserver facilityReserver, IInspectionDAO inspectionDAO) {
        this.facilityReserver = facilityReserver;
        this.inspectionDAO = inspectionDAO;
    }

    @Override
    public boolean IsInUseDuringInterval(IFacility facility, TimeStamp start, TimeStamp end) {
        return facilityReserver.inUse(facility, start, end);
    }

    @Override
    public void assignFacilityToUse(IReservation reservation) {
        facilityReserver.addReservation(reservation);
    }

    @Override
    public void vacateFacility(IFacility facility) {
        facilityReserver.removeAllReservations(facility);
    }

    @Override
    public List<IInspection> listInspections(IFacility facility) {
        return inspectionDAO.getAllInspections(facility);
    }

    @Override
    public List<IReservation> listReservations(IFacility facility) {
        return facilityReserver.listAllReservations(facility);
    }

    @Override
    public int calcUsageRate(IFacility facility) {
        return facilityReserver.usageRate(facility);
    }

    @Override
    public void setInspectionDAO(IInspectionDAO inspectionDAO) {
        this.inspectionDAO = inspectionDAO;
    }

    @Override
    public IInspectionDAO getInspectionDAO() {
        return this.inspectionDAO;
    }

    @Override
    public void setFacilityReserver(IFacilityReserver reserver) {
        this.facilityReserver = reserver;
    }

    @Override
    public IFacilityReserver getFacilityReserver() {
        return this.facilityReserver;
    }
}
