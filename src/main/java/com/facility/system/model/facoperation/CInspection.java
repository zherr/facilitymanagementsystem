package com.facility.system.model.facoperation;

import com.facility.system.model.facoverview.IFacility;

/**
 * Created by zherr on 1/27/14.
 */
public class CInspection implements IInspection{

    private int inspectId;
    private String inspectionName;
    // The facility being inspected
    private IFacility inspectFacility;

    public CInspection() {}

    public CInspection(int inspectId, String inspectionName, IFacility inspectFacility) {
        this.inspectId = inspectId;
        this.inspectionName = inspectionName;
        this.inspectFacility = inspectFacility;
    }

    @Override
    public int getInspectId() {
        return this.inspectId;
    }

    @Override
    public String getInspectionName() {
        return this.inspectionName;
    }

    @Override
    public IFacility getInspectFacility() {
        return this.inspectFacility;
    }

    @Override
    public void setInspectId(int id) {
        this.inspectId = id;
    }

    @Override
    public void setInspectionName(String name) {
        this.inspectionName = name;
    }

    @Override
    public void setInspectFacility(IFacility facility) {
        this.inspectFacility = facility;
    }
}
