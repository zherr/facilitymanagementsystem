package com.facility.system.model.utility;

/**
 * Created with IntelliJ IDEA.
 * User: zherr
 * Date: 9/30/13
 * Time: 9:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class Time {

    private int hours;
    private int minutes;
    private int seconds;
    private int mSecs;

    public Time() {}

    public Time(int hours, int minutes, int seconds, int mSecs) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        this.mSecs = mSecs;
    }

    public int getHours() {
        return this.hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return this.minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSeconds() {
        return this.seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public int getmSecs() {
        return this.mSecs;
    }

    public void setmSecs(int mSecs) {
        this.mSecs = mSecs;
    }

    @Override
    public String toString() {
        String finalString = "";
            finalString += (this.hours < 10) ? ("0" + this.hours) : this.hours;
            finalString += ":";
            finalString += (this.minutes < 10) ? ("0" + this.minutes) : this.minutes;
            finalString += ":";
            finalString += (this.seconds < 10) ? ("0" + this.seconds) : this.seconds;
        return finalString;
    }
}
