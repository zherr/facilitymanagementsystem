package com.facility.system.model.facmaint;

import com.facility.system.model.facoverview.IFacility;

/**
 * Created by zherr on 1/27/14.
 */
public class AMaintenanceRequest implements IMaintenanceRequest{

    protected int maintReqId;
    protected IFacility facilityRequested;
    protected IMaintenance maintenance;
    protected String timeRequested;
    protected boolean isDone;

    /**
     * Default constructor
     */
    protected AMaintenanceRequest() {}

    protected AMaintenanceRequest(int id, IFacility facility, IMaintenance maintenance, String timeRequested, boolean isDone){
        this.maintReqId = id;
        this.facilityRequested = facility;
        this.maintenance = maintenance;
        this.timeRequested = timeRequested;
        this.isDone = isDone;
    }

    @Override
    public int getMaintReqId() {
        return this.maintReqId;
    }

    @Override
    public IFacility getFacilityRequested() {
        return this.facilityRequested;
    }

    @Override
    public IMaintenance getMaintenance() {
        return this.maintenance;
    }

    @Override
    public String getTimeRequested() {
        return this.timeRequested;
    }

    @Override
    public boolean getIsDone() {
        return isDone;
    }

    @Override
    public void setMaintReqId(int id) {
        this.maintReqId = id;
    }

    @Override
    public void setIsDone(boolean done) {
        this.isDone = done;
    }

    @Override
    public void setFacilityRequested(IFacility facilityRequested) {
        this.facilityRequested = facilityRequested;
    }

    @Override
    public void setMaintenance(IMaintenance maintenance) {
        this.maintenance = maintenance;
    }

    @Override
    public void setTimeRequested(String timeRequested) {
        this.timeRequested = timeRequested;
    }
}
