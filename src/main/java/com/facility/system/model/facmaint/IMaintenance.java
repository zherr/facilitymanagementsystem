package com.facility.system.model.facmaint;

/**
 * Created by zherr on 1/27/14.
 */
public interface IMaintenance {
    public int getMaintId();
    public int getCostOfMaintenance();
    public String getMaintenanceDescription();
    public String getMaintenanceStart();
    public String getMaintenanceEnd();
    public void setMaintId(int id);
    public void setCostOfMaintenance(int costOfMaintenance);
    public void setMaintenanceDescription(String maintenanceDescription);
    public void setMaintenanceStart(String maintenanceStart);
    public void setMaintenanceEnd(String maintenanceEnd);
}
