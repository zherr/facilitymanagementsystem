package com.facility.system.model.facmaint;

/**
 * Created by zherr on 1/27/14.
 */
public class CGeneralMaintenance implements IMaintenance{

    private int maintId;
    private int costOfMaintenance;
    private String maintenanceDescription;
    private String maintenanceStart;
    private String maintenanceEnd;

    public CGeneralMaintenance() {}

    public CGeneralMaintenance(int id, int cost, String descr, String start, String end){
        this.maintId = id;
        this.costOfMaintenance = cost;
        this.maintenanceDescription = descr;
        this.maintenanceStart = start;
        this.maintenanceEnd = end;
    }

    @Override
    public int getMaintId() {
        return this.maintId;
    }

    @Override
    public int getCostOfMaintenance() {
        return this.costOfMaintenance;
    }

    @Override
    public String getMaintenanceDescription() {
        return this.maintenanceDescription;
    }

    @Override
    public String getMaintenanceStart() {
        return this.maintenanceStart;
    }

    @Override
    public String getMaintenanceEnd() {
        return this.maintenanceEnd;
    }

    @Override
    public void setMaintId(int id) {
        this.maintId = id;
    }

    @Override
    public void setCostOfMaintenance(int costOfMaintenance) {
        this.costOfMaintenance = costOfMaintenance;
    }

    @Override
    public void setMaintenanceDescription(String maintenanceDescription) {
        this.maintenanceDescription = maintenanceDescription;
    }

    @Override
    public void setMaintenanceStart(String maintenanceStart) {
        this.maintenanceStart = maintenanceStart;
    }

    @Override
    public void setMaintenanceEnd(String maintenanceEnd) {
        this.maintenanceEnd = maintenanceEnd;
    }

}
