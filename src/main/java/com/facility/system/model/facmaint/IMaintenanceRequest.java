package com.facility.system.model.facmaint;

import com.facility.system.model.facoverview.IFacility;

/**
 * Created by zherr on 1/27/14.
 */
public interface IMaintenanceRequest {
    public int getMaintReqId();
    public IFacility getFacilityRequested();
    public IMaintenance getMaintenance();
    public String getTimeRequested();
    public boolean getIsDone();
    public void setMaintReqId(int id);
    public void setIsDone(boolean done);
    public void setFacilityRequested(IFacility facilityRequested);
    public void setMaintenance(IMaintenance maintenance);
    public void setTimeRequested(String timeRequested);
}
