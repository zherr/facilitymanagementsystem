package com.facility.system.model.facmaint;

import com.facility.system.dal.IMaintenanceDAO;
import com.facility.system.dal.IMaintenanceRequestDAO;
import com.facility.system.model.facoverview.IFacility;

import java.util.List;

/**
 * Created by zherr on 1/27/14.
 * Interface for facility maintainers. Generally responsible for maintenance request scheduling
 */
public interface IFacilityMaintainer {
    public void makeFacilityMaintRequest(IMaintenanceRequest maintenanceRequest);
    public void scheduleMaintenance(IMaintenanceRequest maintenanceRequest);
    public float calcMaintenanceCostForFacility(IFacility facility);
    public int calcDownTimeForFacility(IFacility facility);
    public List<IMaintenanceRequest> listMaintRequest(IFacility facility);
    public List<IMaintenance> listMaintenance(IFacility facility);
    public List<IMaintenance> listFacilityProblems(IFacility facility);
    public void setMaintenanceScheduler(IMaintenanceScheduler scheduler);
    public IMaintenanceScheduler getMaintenanceScheduler();
    public void setMaintenanceRequestDAO(IMaintenanceRequestDAO maintReqDAO);
    public IMaintenanceRequestDAO getMaintenanceRequestDAO();
    public void setMaintenanceDAO(IMaintenanceDAO maintDAO);
    public IMaintenanceDAO getMaintenanceDAO();
}
