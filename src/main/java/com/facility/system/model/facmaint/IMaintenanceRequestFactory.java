package com.facility.system.model.facmaint;

import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.utility.TimeStamp;

/**
 * Created by zherr on 1/27/14.
 * Interface for maintenance request factory
 */
public interface IMaintenanceRequestFactory {
    public IMaintenanceRequest createMaintenanceRequest(int id, IFacility facility, IMaintenance maintenance, TimeStamp time, boolean isDone);
}
