package com.facility.system.model.facmaint;

import com.facility.system.dal.IMaintenanceRequestDAO;

import java.util.List;

/**
 * Created by zherr on 1/27/14.
 */
public class CGeneralMaintScheduler extends AMaintenanceScheduler {

    public CGeneralMaintScheduler() {}

    public CGeneralMaintScheduler(List<IMaintenanceRequest> maintenanceRequestQueue, List<IMaintenanceRequest> maintenanceRequestHistory,
                                  IMaintenanceRequestDAO maintReqDAO) {
        super(maintenanceRequestQueue, maintenanceRequestHistory, maintReqDAO);
    }

    /**
     * Add a maintenance request to the queue and DB.
     * @param maintenanceRequest - the maintenance request to schedule
     */
    @Override
    public void scheduleMaintenance(IMaintenanceRequest maintenanceRequest) {
        // make sure it isn't done
        maintenanceRequest.setIsDone(false);
        maintenanceRequestDAO.addMaintenanceRequest(maintenanceRequest);
        maintenanceRequestQueue.add(maintenanceRequest);
    }

    /**
     * "Do" the maintenance in the queue and add it to history, removing it from the queue
     * @param maintenanceRequest - the maintenance request to do
     */
    @Override
    public void doMaintenance(IMaintenanceRequest maintenanceRequest) {
        // make sure it is done
        maintenanceRequest.setIsDone(true);
        maintenanceRequestDAO.addMaintenanceRequestToHistory(maintenanceRequest);
        maintenanceRequestHistory.add(maintenanceRequest);
        maintenanceRequestQueue.remove(maintenanceRequest);
        maintenanceRequestDAO.updateMaintenanceRequest(maintenanceRequest);
    }
}
