package com.facility.system.model.facmaint;

import com.facility.system.dal.IMaintenanceRequestDAO;

import java.util.List;

/**
 * Created by zherr on 1/27/14.
 */
public abstract class AMaintenanceScheduler implements IMaintenanceScheduler{

    protected List<IMaintenanceRequest> maintenanceRequestQueue;
    protected List<IMaintenanceRequest> maintenanceRequestHistory;
    protected IMaintenanceRequestDAO maintenanceRequestDAO;

    /**
     * Default constructor
     */
    protected AMaintenanceScheduler() {}

    protected AMaintenanceScheduler(List<IMaintenanceRequest> maintenanceRequestQueue, List<IMaintenanceRequest> maintenanceRequestHistory,
                                    IMaintenanceRequestDAO maintReqDAO) {
        this.maintenanceRequestQueue = maintenanceRequestQueue;
        this.maintenanceRequestHistory = maintenanceRequestHistory;
        this.maintenanceRequestDAO = maintReqDAO;
    }

    @Override
    public List<IMaintenanceRequest> getMaintenanceRequestQueue() {
        return maintenanceRequestQueue;
    }

    @Override
    public void setMaintenanceRequestQueue(List<IMaintenanceRequest> maintenanceRequestQueue) {
        this.maintenanceRequestQueue = maintenanceRequestQueue;
    }

    @Override
    public List<IMaintenanceRequest> getMaintenanceRequestHistory() {
        return maintenanceRequestHistory;
    }

    @Override
    public void setMaintenanceRequestHistory(List<IMaintenanceRequest> maintenanceRequestHistory) {
        this.maintenanceRequestHistory = maintenanceRequestHistory;
    }

    @Override
    public void setMaintenanceRequestDAO(IMaintenanceRequestDAO maintReqDAO) {
        this.maintenanceRequestDAO = maintReqDAO;
    }

    @Override
    public IMaintenanceRequestDAO getMaintenanceRequestDAO() {
        return this.maintenanceRequestDAO;
    }
}
