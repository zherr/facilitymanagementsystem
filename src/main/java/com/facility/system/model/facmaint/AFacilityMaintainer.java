package com.facility.system.model.facmaint;

import com.facility.system.dal.IMaintenanceDAO;
import com.facility.system.dal.IMaintenanceRequestDAO;

/**
 * Created by zherr on 1/27/14.
 */
public abstract class AFacilityMaintainer implements IFacilityMaintainer{
    protected IMaintenanceScheduler maintenanceScheduler;
    protected IMaintenanceRequestDAO maintenanceRequestDAO;
    protected IMaintenanceDAO maintenanceDAO;

    /**
     * Default constructor
     */
    protected AFacilityMaintainer() {}

    protected AFacilityMaintainer(IMaintenanceScheduler maintenanceScheduler, IMaintenanceRequestDAO maintenanceRequestDAO, IMaintenanceDAO maintenanceDAO) {
        this.maintenanceScheduler = maintenanceScheduler;
        this.maintenanceRequestDAO = maintenanceRequestDAO;
        this.maintenanceDAO = maintenanceDAO;
    }

    @Override
    public IMaintenanceDAO getMaintenanceDAO() {
        return this.maintenanceDAO;
    }

    @Override
    public void setMaintenanceScheduler(IMaintenanceScheduler scheduler) {
        this.maintenanceScheduler = scheduler;
    }

    @Override
    public IMaintenanceScheduler getMaintenanceScheduler() {
        return this.maintenanceScheduler;
    }

    @Override
    public void setMaintenanceRequestDAO(IMaintenanceRequestDAO maintReqDAO) {
        this.maintenanceRequestDAO = maintReqDAO;
    }

    @Override
    public IMaintenanceRequestDAO getMaintenanceRequestDAO() {
        return this.maintenanceRequestDAO;
    }

    @Override
    public void setMaintenanceDAO(IMaintenanceDAO maintDAO) {
        this.maintenanceDAO = maintDAO;
    }
}
