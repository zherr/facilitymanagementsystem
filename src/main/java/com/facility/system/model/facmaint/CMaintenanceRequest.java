package com.facility.system.model.facmaint;

import com.facility.system.model.facoverview.IFacility;

/**
 * Created by zherr on 1/27/14.
 */
public class CMaintenanceRequest extends AMaintenanceRequest{

    public CMaintenanceRequest() {}

    public CMaintenanceRequest(int id, IFacility facility, IMaintenance maintenance, String timeRequested, boolean isDone) {
        super(id, facility, maintenance, timeRequested, isDone);
    }
}
