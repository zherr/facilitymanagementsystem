package com.facility.system.model.facmaint;

import com.facility.system.dal.IMaintenanceRequestDAO;

import java.util.List;

/**
 * Created by zherr on 1/27/14.
 * Interface for maintenance scheduler
 */
public interface IMaintenanceScheduler {
    public void scheduleMaintenance(IMaintenanceRequest maintenanceRequest);
    public void doMaintenance(IMaintenanceRequest maintenanceRequest);
    public List<IMaintenanceRequest> getMaintenanceRequestQueue();
    public void setMaintenanceRequestQueue(List<IMaintenanceRequest> maintenanceRequestQueue);
    public List<IMaintenanceRequest> getMaintenanceRequestHistory();
    public void setMaintenanceRequestHistory(List<IMaintenanceRequest> maintenanceRequestHistory);
    public void setMaintenanceRequestDAO(IMaintenanceRequestDAO maintReqDAO);
    public IMaintenanceRequestDAO getMaintenanceRequestDAO();
}