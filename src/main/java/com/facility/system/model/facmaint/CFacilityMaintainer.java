package com.facility.system.model.facmaint;

import com.facility.system.dal.IMaintenanceDAO;
import com.facility.system.dal.IMaintenanceRequestDAO;
import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.utility.TimeStamp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zherr on 1/27/14.
 */
public class CFacilityMaintainer extends AFacilityMaintainer{

    public CFacilityMaintainer() {}

    public CFacilityMaintainer(IMaintenanceScheduler maintenanceScheduler, IMaintenanceRequestDAO maintenanceRequestDAO, IMaintenanceDAO maintenanceDAO) {
        super(maintenanceScheduler, maintenanceRequestDAO, maintenanceDAO);
    }

    @Override
    public void makeFacilityMaintRequest(IMaintenanceRequest maintenanceRequest) {
        this.maintenanceScheduler.scheduleMaintenance(maintenanceRequest);
    }

    @Override
    public void scheduleMaintenance(IMaintenanceRequest maintenanceRequest) {
        this.maintenanceScheduler.doMaintenance(maintenanceRequest);
    }

    /**
     * Calculates the CURRENT cost of maintenance ofr a facility. (AKA current queue)
     * @param facility - The facility to calc cost of maint for
     * @return The sum of the cost
     */
    @Override
    public float calcMaintenanceCostForFacility(IFacility facility) {
        List<IMaintenance> maints = new ArrayList<IMaintenance>();
        maints = this.maintenanceDAO.listAllMaintenance(facility);
        float sum = 0;
        for(IMaintenance maint : maints){
            sum += maint.getCostOfMaintenance();
        }
        return sum;
    }

    /**
     * Gets the down time for a facility due to maintenance
     * @param facility - The facility to calculate down time for
     * @return The down time for a facility
     */
    @Override
    public int calcDownTimeForFacility(IFacility facility) {
        List<IMaintenance> maints = new ArrayList<IMaintenance>();
        maints = this.maintenanceDAO.listAllMaintenance(facility);
        int downTime = 0;
        for(IMaintenance maint : maints){
            // assuming 24 hour time
            TimeStamp end = new TimeStamp(maint.getMaintenanceEnd().toString());
            TimeStamp start = new TimeStamp(maint.getMaintenanceStart().toString());
            int n = end.getTime().getHours() - start.getTime().getHours();
            if(n < 0){
                n *= -1;
            }
            downTime += n;
        }
        return downTime;
    }

    /**
     * Gets all of the maintenance requests in the queue
     * @param facility - the facility to get maint requests for
     * @return A list of maint requests
     */
    @Override
    public List<IMaintenanceRequest> listMaintRequest(IFacility facility) {
        return this.maintenanceRequestDAO.getAllMaintenanceRequest(facility);
    }

    /**
     * Gets all the maintenance that a facility has had done
     * @param facility - the facility to find maintenance history for
     * @return A list of maintenance done on the facility
     */
    @Override
    public List<IMaintenance> listMaintenance(IFacility facility) {
        List<IMaintenanceRequest> maintenanceRequests = this.maintenanceRequestDAO.getAllMaintenanceRequestsFromHistory(facility);
        List<IMaintenance> maintenances = new ArrayList<IMaintenance>();
        for(IMaintenanceRequest maint : maintenanceRequests){
            maintenances.add(maint.getMaintenance());
        }
        return maintenances;
    }

    @Override
    public List<IMaintenance> listFacilityProblems(IFacility facility) {
        List<IMaintenanceRequest> maintenanceRequests = this.maintenanceRequestDAO.getAllMaintenanceRequestsCurrent(facility);
        List<IMaintenance> maintenances = new ArrayList<IMaintenance>();
        for(IMaintenanceRequest maint : maintenanceRequests){
            maintenances.add(maint.getMaintenance());
        }
        return maintenances;
    }
}
