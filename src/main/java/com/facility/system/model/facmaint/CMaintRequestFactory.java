package com.facility.system.model.facmaint;

import com.facility.system.model.facoverview.IFacility;
import com.facility.system.model.utility.TimeStamp;

/**
 * Created by zherr on 1/27/14.
 * Concrete implementation for maintenance request factory
 */
public class CMaintRequestFactory implements IMaintenanceRequestFactory{

    public CMaintRequestFactory() {}

    @Override
    public IMaintenanceRequest createMaintenanceRequest(int id, IFacility facility, IMaintenance maintenance, TimeStamp time, boolean isDone) {
        return new CMaintenanceRequest(id ,facility, maintenance, time.toString(), isDone);
    }
}
