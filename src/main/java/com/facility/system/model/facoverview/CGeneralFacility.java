package com.facility.system.model.facoverview;

/**
 * Created by zherr on 1/26/14.
 * Concrete facility class
 */
public class CGeneralFacility extends AFacility {

    public CGeneralFacility() {}

    public CGeneralFacility(int facilityId, String facilityName, String facilityDescription, String facilityLocation, int totalCapacity, int availableCapacity) {
        super(facilityId, facilityName, facilityDescription, facilityLocation, totalCapacity, availableCapacity);
    }
}
