package com.facility.system.model.facoverview;

import com.facility.system.dal.IFacilityDAO;
import com.facility.system.model.user.COwner;

import java.util.List;

/**
 * Created by zherr on 1/26/14.
 * Concrete implementation for facility overseer
 */
public class CFacilityOverseer extends AFacilityOverseer{

    public CFacilityOverseer() {}

    public CFacilityOverseer(IFacilityDAO facilityDAO) {
        super(facilityDAO);
    }

    @Override
    public List<IFacility> listFacilities() {
        return facilityDAO.getAllFacilities();
    }

    /**
     * Gets a simple string view of a facility's information
     * @param fac - The facility to get information about
     * @return A string representation of facility information
     */
    @Override
    public String getFacilityInformation(IFacility fac) {
        String info = "";
        info += fac.getFacilityId() + "\n" + fac.getFacilityName() + "\n" + fac.getFacilityDescription() + "\nLocation: " + fac.getFacilityLocation()
                + "\n Total Capacity: " + fac.getTotalCapacity() + "\nAvailable Capacity: " + fac.getAvailableCapacity();
        return info;
    }

    @Override
    public int requestAvailableCapacity(IFacility fac) {
        return fac.getAvailableCapacity();
    }

    @Override
    public void addNewFacility(IFacility fac) {
        facilityDAO.addFacility(fac);
    }

    @Override
    public void addFacilityDetail(int facId, String facName, String facDescr, String location, int availCap, int totalCapacity, COwner owner) {
        IFacility fac = new COwnedFacility(facId, facName, facDescr, location, availCap, totalCapacity, owner);
        facilityDAO.updateFacility(fac);
    }

    @Override
    public void removeFacility(IFacility fac) {
        facilityDAO.deleteFacility(fac);
    }
}
