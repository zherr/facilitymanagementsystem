package com.facility.system.model.facoverview;

/**
 * Created by zherr on 1/26/14.
 * Interface for facilities
 */
public interface IFacility {
    public int getFacilityId();
    public String getFacilityName();
    public String getFacilityDescription();
    public String getFacilityLocation();
    public int getTotalCapacity();
    public int getAvailableCapacity();
    public void setFacilityId(int facilityId);
    public void setFacilityName(String facilityName);
    public void setFacilityDescription(String facilityDescription);
    public void setFacilityLocation(String facilityLocation);
    public void setTotalCapacity(int totalCapacity);
    public void setAvailableCapacity(int availableCapacity);
}
