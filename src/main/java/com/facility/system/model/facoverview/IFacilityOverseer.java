package com.facility.system.model.facoverview;

import com.facility.system.dal.IFacilityDAO;
import com.facility.system.model.user.COwner;

import java.util.List;

/**
 * Created by zherr on 1/26/14.
 * Interface for a facility overseer
 */
public interface IFacilityOverseer {
    public List<IFacility> listFacilities();
    public String getFacilityInformation(IFacility fac);
    public int requestAvailableCapacity(IFacility fac);
    public void addNewFacility(IFacility fac);
    public void addFacilityDetail(int facId, String facName, String facDescr, String location, int availCap, int totalCapacity, COwner owner);
    public void removeFacility(IFacility fac);
    public void setFacilityDAO(IFacilityDAO facilityDAO);
    public IFacilityDAO getFacilityDAO();
}
