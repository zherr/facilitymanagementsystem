package com.facility.system.model.facoverview;

        import com.facility.system.model.user.COwner;

/**
 * Created by zherr on 2/21/14.
 */
public class COwnedFacility extends AFacility {

    private COwner owner;

    public COwnedFacility() {}

    public COwnedFacility(int facilityId, String facilityName, String facilityDescription, String facilityLocation, int totalCapacity, int availableCapacity, COwner owner) {
        super(facilityId, facilityName, facilityDescription, facilityLocation, totalCapacity, availableCapacity);
        this.owner = owner;
    }

    public COwner getOwner() {
        return this.owner;
    }

    public void setOwner(COwner owner) {
        this.owner = owner;
    }
}
