package com.facility.system.model.facoverview;

/**
 * Created by zherr on 1/26/14.
 * Abstract class for default behavior for facilities
 */
public abstract class AFacility implements IFacility{

    private int facilityId;
    private String facilityName;
    private String facilityDescription;
    private String facilityLocation;
    private int totalCapacity;
    private int availableCapacity;

    /**
     * Default constructor
     */
    protected AFacility() {}

    protected AFacility(int facilityId, String facilityName, String facilityDescription, String facilityLocation, int totalCapacity, int availableCapacity) {
        this.facilityId = facilityId;
        this.facilityName = facilityName;
        this.facilityDescription = facilityDescription;
        this.facilityLocation = facilityLocation;
        this.totalCapacity = totalCapacity;
        this.availableCapacity = availableCapacity;
    }

    public int getFacilityId(){
        return this.facilityId;
    }

    public String getFacilityName(){
        return this.facilityName;
    }

    public String getFacilityDescription(){
        return this.facilityDescription;
    }

    public String getFacilityLocation(){
        return this.facilityLocation;
    }

    public int getTotalCapacity(){
        return this.totalCapacity;
    }

    public int getAvailableCapacity(){
        return this.availableCapacity;
    }

    public void setFacilityId(int facilityId) {
        this.facilityId = facilityId;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public void setFacilityDescription(String facilityDescription) {
        this.facilityDescription = facilityDescription;
    }

    public void setFacilityLocation(String facilityLocation) {
        this.facilityLocation = facilityLocation;
    }

    public void setTotalCapacity(int totalCapacity) {
        this.totalCapacity = totalCapacity;
    }

    public void setAvailableCapacity(int availableCapacity) {
        this.availableCapacity = availableCapacity;
    }
}
