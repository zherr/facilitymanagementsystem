package com.facility.system.model.facoverview;

import com.facility.system.dal.IFacilityDAO;

/**
 * Created by zherr on 1/26/14.
 * Abstract class for default behavior for facility overseer
 */
public abstract class AFacilityOverseer implements IFacilityOverseer{
    protected IFacilityDAO facilityDAO;

    protected AFacilityOverseer() {}

    protected AFacilityOverseer(IFacilityDAO facilityDAO) {
        this.facilityDAO = facilityDAO;
    }

    @Override
    public void setFacilityDAO(IFacilityDAO facilityDAO) {
        this.facilityDAO = facilityDAO;
    }

    @Override
    public IFacilityDAO getFacilityDAO() {
        return this.facilityDAO;
    }
}
